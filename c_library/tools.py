import numpy as np
import ctypes
import os

# pick the same elements in two integer arrays
def match(x, y, bool=False):
    
    a = np.copy(x); a = np.array(a, dtype='int32')
    b = np.copy(y); b = np.array(b, dtype='int32')
    aindex = np.argsort(a); asorted = a[aindex]; alen = len(a)
    bindex = np.argsort(b); bsorted = b[bindex]; blen = len(b)
    
    if ((alen==0) | (blen==0)):
        subx = np.array([], dtype='int32'); suby = np.array([],dtype='int32')
    else:
        asorted_ctype = asorted.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        bsorted_ctype = bsorted.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        alen_ctype = ctypes.c_int(alen)
        blen_ctype = ctypes.c_int(blen)
        suba_ctype = (ctypes.c_int*alen)()
        subb_ctype = (ctypes.c_int*blen)()
        
        exec_call = os.environ['PYLIB'] + "/c_library/tools/tools.so"
        match_routine = ctypes.cdll[exec_call]
        match_routine.match(asorted_ctype, bsorted_ctype,
                            alen_ctype, blen_ctype,
                            suba_ctype, subb_ctype)
            
        suba = np.ctypeslib.as_array(suba_ctype).reshape(-1)
        subb = np.ctypeslib.as_array(subb_ctype).reshape(-1)
        subx = aindex[suba>0]; suby = bindex[subb>0]

    if (bool):
        xbool = np.zeros(alen, dtype='bool')
        ybool = np.zeros(blen, dtype='bool')
        xbool[subx] = True; ybool[suby] = True
        return xbool, ybool
    
    return subx, suby
