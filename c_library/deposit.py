import numpy as np
import ctypes
import os


# this is the interface with my C functions
def deposit(x,y,z=None,h=None,wt=None,le=None,re=None,N=None,method='simple',DO_3D=1):

    # set boundaries and grid size if not given
    ## boundary not given, calculate from coordinates
    if (le is None) or (re is None):
        
        xc, yc = np.median(x), np.median(y)
        Lx = np.max(np.abs(x-xc))
        Ly = np.max(np.abs(y-yc))
        
        if (DO_3D==1) and (z is not None):
            zc = np.median(z)
            Lz = np.max(np.abs(z-zc))
            L = max(Lx,Ly,Lz)
            le, re = [xc-L,yc-L,zc-L], [xc+L,yc+L,zc+L]
        else:
            L = max(Lx,Ly)
            le, re = [xc-L,yc-L], [xc+L,yc+L]

    ## boundary is given, but not grid size
    if N is None:
        
        xl, yl, xr, yr = le[0], le[1], re[0], re[1]
        Lx, Ly = xr-xl, yr-rl
    
        if (DO_3D==1) and (z is not None):
            zl, zr =  le[2], re[2]
            Lz = zr-zl
            L = max(Lx,Ly,Lz)
            N = [int(100.0*Lx/L),int(100.0*Ly/L),int(100.0*Lz/L)]
        else:
            L = max(Lx,Ly)
            N = [int(100.0*Lx/L),int(100.0*Ly/L)]

    # convert data type, these fields are always required
    ## coordinates
    Npart = ctypes.c_int(np.int32(len(x)))
    x = np.array(x, dtype='float64')
    y = np.array(y, dtype='float64')
    posx = x.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    posy = y.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    if (DO_3D==1) and (z is not None):
        z = np.array(z, dtype='float64')
        posz = z.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

    ## boundary and grid size
    le = np.array(le, dtype='float64')
    re = np.array(re, dtype='float64')
    N = np.array(N, dtype='int32')
    left_edge = le.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    right_edge = re.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    dims = N.ctypes.data_as(ctypes.POINTER(ctypes.c_int))

    ## C routine to call
    exec_call = os.environ['PYLIB']+'/c_library/deposit/deposit.so'
    routine = ctypes.cdll[exec_call]

    # now, call the routines
    ## try smooth deposit, need h and wt
    if (method=='smooth') and (h is not None) and (wt is not None):
        
        h = np.array(h, dtype='float64')
        wt = np.array(wt, dtype='float64')
        hsml = h.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        field = wt.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        
        if (DO_3D==1) and (z is not None):
            grid_out_format = ctypes.c_double*N[2]*N[1]*N[0]
        else:
            grid_out_format = ctypes.c_double*N[1]*N[0]
        data_ctype = grid_out_format()
        temp_ctype = grid_out_format()

        if (DO_3D==1) and (z is not None):
            routine.smooth3d(Npart,posx,posy,posz,hsml,field,left_edge,right_edge,dims,data_ctype,temp_ctype)
        else:
            routine.smooth2d(Npart,posx,posy,hsml,field,left_edge,right_edge,dims,data_ctype,temp_ctype)

        data = np.ctypeslib.as_array(data_ctype)

        return data

    else:
        
        method = 'simple'
    
    ##  try simple deposit, need wt
    if (method=='simple') and (wt is not None):
        
        wt = np.array(wt, dtype='float64')
        field = wt.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        
        if (DO_3D==1) and (z is not None):
            grid_out_format = ctypes.c_double*N[2]*N[1]*N[0]
        else:
            grid_out_format = ctypes.c_double*N[1]*N[0]
        data_ctype = grid_out_format()

        if (DO_3D==1) and (z is not None):
            routine.simple3d(Npart,posx,posy,posz,field,left_edge,right_edge,dims,data_ctype)
        else:
            routine.simple2d(Npart,posx,posy,field,left_edge,right_edge,dims,data_ctype)

        data = np.ctypeslib.as_array(data_ctype)

    ## finally, number counts, need nothing but coordinates
    else:
        
        if (DO_3D==1) and (z is not None):
            grid_out_format = ctypes.c_int*N[2]*N[1]*N[0]
        else:
            grid_out_format = ctypes.c_int*N[1]*N[0]
        data_ctype = grid_out_format()
        
        if (DO_3D==1) and (z is not None):
            routine.count3d(Npart,posx,posy,posz,left_edge,right_edge,dims,data_ctype)
        else:
            routine.count2d(Npart,posx,posy,left_edge,right_edge,dims,data_ctype)

        data = np.ctypeslib.as_array(data_ctype)

    return data
