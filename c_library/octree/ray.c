#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvar.h"
#include "proto.h"


void compute_column_density(int N_star, double* Pos, int N_los, double* thetas, double* phis, double rmax, char* fname, double* OUT_NH, double* OUT_NH_nh, double* OUT_NH_z)
{
    // read octree
    read_tree_from_file(fname);
    
    // do the calculation
    int j; double theta, phi;
    long los_num, ray_num;
    for (los_num=0; los_num<N_los; los_num++)
    {
        // direction for current sightline
        theta = *(thetas+los_num);
        phi = *(phis+los_num);
        if (sin(theta)==0) theta += 1e-10;
        if (cos(theta)==0) theta += 1e-10;
        if (sin(phi)==0) phi += 1e-10;
        if (cos(phi)==0) phi += 1e-10; // ensure it is not align with axis
        printf("Calculate sightline %ld, theta=%.2f, phi=%.2f\n", los_num, theta, phi);
        
        for (ray_num=0; ray_num<N_star; ray_num++)
        {
            // initialize ray
            Ray_struct ray;
            for (j=0;j<3;j++) ray.pos[j] = *(Pos+3*ray_num+j);
            ray.theta = theta;
            ray.phi = phi;
            ray.n_hat[0] = sin(ray.theta)*cos(ray.phi);
            ray.n_hat[1] = sin(ray.theta)*sin(ray.phi);
            ray.n_hat[2] = cos(ray.theta);
            ray.rmax = (rmax>0)? rmax : 1e3*MAX_DISTANCE_FROM0;
            ray.cell_ID = find_cell_from_cell(ray.pos,0);
            if (ray.cell_ID>=0)
            {
                ray.NH = 0;
                ray.NH_z = 0;
                ray.NH_nh = 0; // inside domain
            }
            else
            {
                ray.NH = -1;
                ray.NH_z = -1;
                ray.NH_nh = -1; // outside domain
            }
            
            // do the integration
            integrate_ray_to_escape(&ray);
            
            if (ray.NH>0)
            {
                ray.NH_z /= ray.NH;
                ray.NH_nh /= ray.NH;
                //ray[ray_num].NH *= 9.5e23; // from code unit to cm^-2
            }
            else
            {
                ray.NH=0;
                ray.NH_z=0;
                ray.NH_nh=1;
            }
            
            // write to output arrays
            *(OUT_NH+(ray_num*N_los)+los_num) = ray.NH;
            *(OUT_NH_nh+(ray_num*N_los)+los_num) = ray.NH_nh;
            *(OUT_NH_z+(ray_num*N_los)+los_num) = ray.NH_z;
        }
        
    }
    
    free_the_tree();
    return;
}


void project_tree_on_image(double* cen, double L, double Lz, int Nx, double theta, double phi, double psi, char* fname, double* OUT_NH, double* OUT_NH_nh, double* OUT_NH_z)
{
    // read octree
    read_tree_from_file(fname);
    
    //if (sin(theta)==0) theta += 1e-10;
    //if (cos(theta)==0) theta += 1e-10;
    //if (sin(phi)==0) phi += 1e-10;
    //if (cos(phi)==0) phi += 1e-10; // ensure it is not align with axis
    
    // do the calculation
    int i,j,ray_num;
    double dx=2*L/Nx;
    double x0,y0,z0;
    
    // matrix to convert coordinates
    double A11 = cos(theta)*cos(phi)*cos(psi) - sin(phi)*sin(psi);
    double A12 = -cos(theta)*cos(phi)*sin(psi) - sin(phi)*cos(psi);
    double A13 = sin(theta)*cos(phi);
    double A21 = cos(theta)*sin(phi)*cos(psi) + cos(phi)*sin(psi);
    double A22 = -cos(theta)*sin(phi)*sin(psi) + cos(phi)*cos(psi);
    double A23 = sin(theta)*sin(phi);
    double A31 = -sin(theta)*cos(psi);
    double A32 = sin(theta)*sin(psi);
    double A33 = cos(theta);
    
    for (i=0;i<Nx;i++)
    {
        for (j=0;j<Nx;j++)
        {
            Ray_struct ray;
            x0 = -L + (i+0.5)*dx;
            y0 = -L + (j+0.5)*dx;
            z0 = -Lz; // coordinate on the image
            ray.pos[0] = A11*x0 + A12*y0 + A13*z0 + cen[0];
            ray.pos[1] = A21*x0 + A22*y0 + A23*z0 + cen[1];
            ray.pos[2] = A31*x0 + A32*y0 + A33*z0 + cen[2];
            ray.rmax = 2*Lz;
            ray.theta = theta;
            ray.phi = phi;
            ray.n_hat[0] = sin(ray.theta)*cos(ray.phi);
            ray.n_hat[1] = sin(ray.theta)*sin(ray.phi);
            ray.n_hat[2] = cos(ray.theta);
            
            initialize_the_ray(&ray);
            integrate_ray_to_escape(&ray);
            
            *(OUT_NH+i*Nx+j) = ray.NH;
            *(OUT_NH_nh+i*Nx+j) = ray.NH_nh;
            *(OUT_NH_z+i*Nx+j) = ray.NH_z;
        }
    }
    
    free_the_tree();
    return;
}


void initialize_the_ray(Ray_struct *ray)
{
    ray->cell_ID = find_cell_from_cell(ray->pos,0);
    
    // check if the ray can enter the domain
    if (ray->cell_ID<0)
    {
        int j; double xyz[3]; double nx[3];
        for (j=0;j<3;j++) {xyz[j]=ray->pos[j]; nx[j]=ray->n_hat[j];}
        
        double L=MAX_DISTANCE_FROM0;
        double eps=1e-10*MAX_DISTANCE_FROM0;
        double amax=1000*MAX_DISTANCE_FROM0; // arbitrarily large
        double xmin,xmax,lmin=0,lmax=amax;
        
        for (j=0;j<3;j++)
        {
            if (nx[j]==0) {xmin=(-L<=xyz[j]||xyz[j]<L)?-amax:amax; xmax=amax;}
            if (nx[j]>0) {xmin=(-L-xyz[j])/nx[j]; xmax=(L-xyz[j])/nx[j];}
            if (nx[j]<0) {xmin=(L-xyz[j])/nx[j]; xmax=(-L-xyz[j])/nx[j];}
            if (lmin<xmin) lmin=xmin;
            if (lmax>xmax) lmax=xmax;
        } // check if the ray can enter the domain eventually
        
        if (lmax>lmin) // move the ray into the domain
        {
            for (j=0;j<3;j++) ray->pos[j]+=(lmin+eps)*ray->n_hat[j];
            ray->cell_ID = find_cell_from_cell(ray->pos,0);
        }
    }
    
    ray->NH = 0;
    ray->NH_z = 0;
    ray->NH_nh = 0;
    
    return;
}


void integrate_ray_to_escape(Ray_struct *ray)
{
    while (ray->cell_ID>=0 && ray->rmax>0)
    {
        long cell_id = ray->cell_ID;
        int j; double dr,rhodr;
        
        dr = maxstep_escape_cell(ray->pos,ray->n_hat,cell_id);
        if (dr>ray->rmax) dr = ray->rmax; // step size
        
        for (j=0; j<3; j++) ray->pos[j] += dr*ray->n_hat[j]; // take step
        rhodr = CELL[cell_id].U->rho*dr;
        ray->NH += rhodr; // add to column
        ray->NH_nh += rhodr*CELL[cell_id].U->nh;
        ray->NH_z += rhodr*CELL[cell_id].U->z;
        ray->rmax -= dr; // remove from rmax
        ray->cell_ID = find_cell_from_cell(ray->pos, cell_id);
    }
    return;
}
