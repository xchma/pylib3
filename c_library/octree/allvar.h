#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define PI 3.14159265358979323846


// gas particle
struct global_data_all_processes
{
    long N_gas;
} All;


struct particle_3d
{
    float Pos[3];
} **P3d; // only used for ngbtree


struct particle_gas
{
    float pos[3];
    float vel[3];
    float mass;
    float hsml;
    float nh;
    float ne;
    float u;
    float z;
} *PG;



// cubic spline kernel
#define KERNEL_TABLE 1000
#define MAXITER 200
/* tabulated smoothing kernel */
double  Kernel[KERNEL_TABLE+2],
        KernelRad[KERNEL_TABLE+2];



// tree parameter
double CENTER[3];
double MAX_DISTANCE_FROM0;
double TREE_MIN_SIZE;
int TREE_MAX_NPART;

#define N_CELL_DIV 2
#define N_SUBCELL   N_CELL_DIV
#define N_SUBCELL_2 N_SUBCELL*N_SUBCELL
#define N_SUBCELL_3 N_SUBCELL*N_SUBCELL*N_SUBCELL

#define NUMBER_OF_NEIGHBORS 32

typedef struct LOCALVALS_s
{
    double vel[3];               /* velocity */
    double rho;				    /* density */
    double nh;                   /* neutral fraction */
    double ne;				    /* electron fraction (number of electrons per H atom) */
    double u;				    /* internal energy per unit mass per particle */
    double z;				    /* metallicity */
    double T;				    /* temperature */
} LOCALVAL;


struct CELL_STRUCT
{
    double width;                    /* width from one edge to another of the cell */
    double min_x[3];                 /* x,y,z minima of the cell */
    
    long parent_ID;                 /* ID number of parent cell for the given cell */
    int sub_cell_check;			/* = 1 if there are sub-cells, 0 if not */
    long *sub_cell_IDs;             /* ID numbers of the sub-cells contained (if present) */
    
    long N_particles_contained;     /* number of SPH particles contained in a cell */
    long *particle_IDs;             /* vector of indices of the particles contained in the cell */
    
    float hsml_min;                 /* minimum smoothing length of contained particles */
    
    LOCALVAL *U;					/* structure to hold local variables of the cell */
    LOCALVAL *dU;
    int HAS_U_ALLOCATED;
    int HAS_dU_ALLOCATED;         /* flag to know if dU was allocated (so it can be properly freed) */
    int HAS_pIDs_ALLOCATED;       /* flag to know if particle_IDs was allocated (so it can be properly freed) */
    
} *CELL;

long TOTAL_NUMBER_OF_CELLS;
long TOTAL_NUMBER_OF_PARENTS;
long TOTAL_NUMBER_OF_LEAVES;
long ALL_CELL_COUNTER;

long *LEAF_IDS;
long *PARENT_IDS;



// ray parameter
typedef struct Ray_s
{
    double pos[3];				/* current position */
    long cell_ID;               /* current cell */
    double rmax;                /* maximum distance to integrate */
    
    double theta;				/* polar angle theta of the ray */
    double phi;                 /* azimuthal angle phi of the ray */
    double n_hat[3];			/* unit vector direction of the ray */
    
    double NH;					/* integrated gas column density */
    double NH_z;				/* mass-weighted metallicity along the ray */
    double NH_nh;               /* mass-weighted neutral fraction along the ray */
} Ray_struct;
