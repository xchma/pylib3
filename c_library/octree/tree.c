#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvar.h"
#include "proto.h"
#include "ngbtree3d.h"


void initialize_the_tree(void){

    long np,i,j,k,in_box_check,N_SUB_ID,nx,ny,nz,n_ID,PID,n_ID_sub,n_cell;
    long n_subshell_i,n_subshell_f,n_x[3];
    
    printf("MAX_NPART = %d, MIN_SIZE = %g\n", TREE_MAX_NPART, TREE_MIN_SIZE);
    
    np = 4*All.N_gas;
	if (!(CELL=malloc(np*sizeof(struct CELL_STRUCT)))){
        fprintf(stderr, "failed to allocate memory (CELL).\n");
	    exit(0);
    }
    
    /* initialize cell */
	for (j=0;j<np;j++) {
        CELL[j].N_particles_contained = 0;
        CELL[j].HAS_pIDs_ALLOCATED = 0; // particle list
        CELL[j].sub_cell_check = -1;
        CELL[j].HAS_U_ALLOCATED = 0;
        CELL[j].HAS_dU_ALLOCATED = 0;
	}
	printf("Number of cells allocated: %ld\n", np);

	ALL_CELL_COUNTER = 1;
    
    /* set up the parent box that includes everything */
	for (j=0;j<3;j++) CELL[0].min_x[j] = -1.0*MAX_DISTANCE_FROM0;
	CELL[0].width = 2.0*MAX_DISTANCE_FROM0;
	CELL[0].sub_cell_check = 1;
    CELL[0].sub_cell_IDs = malloc(N_SUBCELL_3*sizeof(long));
	CELL[0].parent_ID = -1;
	for (nx=0;nx<N_SUBCELL;nx++) {
	for (ny=0;ny<N_SUBCELL;ny++) {
	for (nz=0;nz<N_SUBCELL;nz++) {
		n_ID_sub = nx*N_SUBCELL_2 + ny*N_SUBCELL + nz;
		n_ID = n_ID_sub + ALL_CELL_COUNTER;
		CELL[n_ID].parent_ID = 0;
		PID = CELL[n_ID].parent_ID;
		CELL[PID].sub_cell_IDs[n_ID_sub] = n_ID;
		CELL[n_ID].width = CELL[PID].width / N_SUBCELL;
		CELL[n_ID].N_particles_contained = 0;

		n_x[0] = nx; n_x[1] = ny; n_x[2] = nz;
		for (j=0;j<3;j++) {
            CELL[n_ID].min_x[j] = CELL[PID].min_x[j] + CELL[n_ID].width * n_x[j];
		}
	}}}
	CELL[0].N_particles_contained = 0;
	
	/* looping over all particles to determine which sectors they go to */
	for (np=0;np<All.N_gas;np++) {
		in_box_check = 1;
		for (j=0;j<3;j++) {
			n_x[j] = (long)((PG[np].pos[j] - CELL[0].min_x[j])/(CELL[0].width/N_SUBCELL));
			if (n_x[j] < 0 || n_x[j] > N_SUBCELL-1) in_box_check = 0;
		}
		if (in_box_check != 0) {
			N_SUB_ID = n_x[0]*N_SUBCELL_2 + n_x[1]*N_SUBCELL + n_x[2];
			CELL[0].N_particles_contained += 1;
			CELL[ALL_CELL_COUNTER + N_SUB_ID].N_particles_contained += 1;
		}
	}

	/* allocate particle ID list */
	for (n_cell=ALL_CELL_COUNTER; n_cell<ALL_CELL_COUNTER+N_SUBCELL_3; n_cell++) {
		CELL[n_cell].hsml_min = 1.0e10;
		if (CELL[n_cell].N_particles_contained > 0) {
            CELL[n_cell].particle_IDs = malloc(CELL[n_cell].N_particles_contained*sizeof(long));
            CELL[n_cell].HAS_pIDs_ALLOCATED = 1;
            CELL[n_cell].N_particles_contained = 0;
	}}

	/* fill in particle list */
	for (np=0;np<All.N_gas;np++) {
		in_box_check = 1;
		for (j=0;j<3;j++) {
			n_x[j] = (long)((PG[np].pos[j] - CELL[0].min_x[j])/(CELL[0].width/N_SUBCELL));
			if (n_x[j] < 0 || n_x[j] > N_SUBCELL-1) in_box_check = 0;
		}
		if (in_box_check != 0) {
			N_SUB_ID = ALL_CELL_COUNTER + n_x[0]*N_SUBCELL_2 + n_x[1]*N_SUBCELL + n_x[2];
			CELL[N_SUB_ID].particle_IDs[CELL[N_SUB_ID].N_particles_contained] = np;
			CELL[N_SUB_ID].N_particles_contained += 1;
			if (PG[np].hsml < CELL[N_SUB_ID].hsml_min) CELL[N_SUB_ID].hsml_min = PG[np].hsml;
		}
	}
	n_subshell_i = ALL_CELL_COUNTER;
	n_subshell_f = ALL_CELL_COUNTER + N_SUBCELL_3;
	ALL_CELL_COUNTER += N_SUBCELL_3;
	
	printf("Tree opened, building... \n");

    for (n_cell=n_subshell_i; n_cell<n_subshell_f; n_cell++) {
        printf("..%ld..",n_cell);
        fflush(stdout);
        build_cell(n_cell);
	}

	printf("\nTree constructed, %ld cells, ", ALL_CELL_COUNTER);
	TOTAL_NUMBER_OF_CELLS = ALL_CELL_COUNTER;
    
    // mass conservation
    tree_conserve_mass();
    
    // get statistics
    tree_statistics();
    
    printf("%ld parents, %ld leaves\n", TOTAL_NUMBER_OF_PARENTS, TOTAL_NUMBER_OF_LEAVES);
    
	return;
}


void build_cell(long cell_ID){
	
	long nx,ny,nz,n_x[3],j,np,p_ID,n_ID_sub,n_ID,n_subshell_i,n_subshell_f,in_box_check;
	
	CELL[cell_ID].sub_cell_check = 1;
	
    /* decide whether it is at the finest level */
    if (CELL[cell_ID].N_particles_contained <= TREE_MAX_NPART)
        CELL[cell_ID].sub_cell_check=0;
	if (CELL[cell_ID].width <= TREE_MIN_SIZE)
        CELL[cell_ID].sub_cell_check=0;
		
	if (CELL[cell_ID].sub_cell_check == 0) {
		get_tree_localvals(cell_ID);
		return;
	}

    /* we need to expand this cell */

	/* first set up the basic variables of the new cell */
    CELL[cell_ID].sub_cell_IDs = malloc(N_SUBCELL_3*sizeof(long));
	for (nx=0;nx<N_SUBCELL;nx++) {
	for (ny=0;ny<N_SUBCELL;ny++) {
	for (nz=0;nz<N_SUBCELL;nz++) {
		n_ID_sub = nx*N_SUBCELL_2 + ny*N_SUBCELL + nz;
		n_ID = n_ID_sub + ALL_CELL_COUNTER;
		CELL[cell_ID].sub_cell_IDs[n_ID_sub] = n_ID;
		CELL[n_ID].parent_ID = cell_ID;
		CELL[n_ID].width = CELL[cell_ID].width / N_SUBCELL;
		CELL[n_ID].N_particles_contained = 0;

		n_x[0] = nx; n_x[1] = ny; n_x[2] = nz;
		for (j=0;j<3;j++)
			CELL[n_ID].min_x[j] = CELL[cell_ID].min_x[j] + CELL[n_ID].width * n_x[j];
	}}}

	/* loop over the gas particles in the cell and determine which sub-cells they belong to */
	for (np=0; np<CELL[cell_ID].N_particles_contained; np++) {
		p_ID = CELL[cell_ID].particle_IDs[np];

		in_box_check = 1;
		for (j=0;j<3;j++) {
			n_x[j] = (long)((PG[p_ID].pos[j] - CELL[cell_ID].min_x[j])/(CELL[cell_ID].width/N_SUBCELL));
			if (n_x[j] < 0 || n_x[j] > N_SUBCELL-1) in_box_check = 0;
		}
		if (in_box_check != 0) {
			n_ID_sub = n_x[0]*N_SUBCELL_2 + n_x[1]*N_SUBCELL + n_x[2];
			n_ID = n_ID_sub + ALL_CELL_COUNTER;
			CELL[n_ID].N_particles_contained += 1;
		}
	}
    
    /* allocate particle list */
	for (n_ID=ALL_CELL_COUNTER; n_ID<ALL_CELL_COUNTER+N_SUBCELL_3; n_ID++) {
		CELL[n_ID].hsml_min = 1.0e10;
		if (CELL[n_ID].N_particles_contained > 0) {
		CELL[n_ID].particle_IDs = malloc(CELL[n_ID].N_particles_contained*sizeof(long));
		CELL[n_ID].HAS_pIDs_ALLOCATED = 1;
		CELL[n_ID].N_particles_contained = 0;
	}}
    
    /* fill the particle list */
	for (np=0; np<CELL[cell_ID].N_particles_contained; np++) {
		p_ID = CELL[cell_ID].particle_IDs[np];
		
		in_box_check = 1;
		for (j=0;j<3;j++) {
			n_x[j] = (long)((PG[p_ID].pos[j] - CELL[cell_ID].min_x[j])/(CELL[cell_ID].width/N_SUBCELL));
			if (n_x[j] < 0 || n_x[j] > N_SUBCELL-1) in_box_check = 0;
		}
		if (in_box_check != 0) {
			n_ID_sub = n_x[0]*N_SUBCELL_2 + n_x[1]*N_SUBCELL + n_x[2];
			n_ID = n_ID_sub + ALL_CELL_COUNTER;

			CELL[n_ID].particle_IDs[CELL[n_ID].N_particles_contained] = p_ID;
			CELL[n_ID].N_particles_contained += 1;
			if (PG[p_ID].hsml < CELL[n_ID].hsml_min) CELL[n_ID].hsml_min = PG[p_ID].hsml;
	}}
    
	n_subshell_i = ALL_CELL_COUNTER;
	n_subshell_f = ALL_CELL_COUNTER + N_SUBCELL_3;
	ALL_CELL_COUNTER += N_SUBCELL_3;
    
	for (n_ID=n_subshell_i; n_ID<n_subshell_f; n_ID++) build_cell(n_ID);
    
	return;
    
}


void get_tree_localvals(long cell_ID)
{
    /* check sub-cell */
    if (CELL[cell_ID].sub_cell_check==1) return;
    
    /* allocate memory for local variable */
    CELL[cell_ID].HAS_U_ALLOCATED = 1;
    CELL[cell_ID].U = (LOCALVAL *)malloc(sizeof(LOCALVAL));
    
    long i,p_ID,ii;
    double rho,u,Z,ne,nh,mu,T,hi,wk,vx,vy,vz;
    long Num_neighbors = (long)NUMBER_OF_NEIGHBORS;
    float *r2_list;
    long *ngb_list;

    float r0[3];
    for (i=0; i<3; i++) r0[i] = CELL[cell_ID].min_x[i] + 0.5*CELL[cell_ID].width;

    float h,h2;
    double hsml,hinv,hinv3,r,x;
    h=CELL[cell_ID].width;
    
    long ngbfound;
    h2 = ngb3d_treefind(r0, Num_neighbors, 1.04*h, &ngb_list, &r2_list, 5*h, &ngbfound);
    hsml = sqrt(h2); hinv = 1.0/hsml; hinv3 = hinv*hinv*hinv;
    
    rho=u=Z=ne=nh=vx=vy=vz=T=0;
    for (i=0; i<ngbfound; i++)
    {
        p_ID = ngb_list[i];
        r = sqrt(r2_list[i]);
        if (r>hsml) r=hsml;
        x = r*hinv;
        ii = (long)(x*KERNEL_TABLE);
        wk = hinv3*(Kernel[ii]+(Kernel[ii+1]-Kernel[ii])*(x-KernelRad[ii])*KERNEL_TABLE);
        
        rho += PG[p_ID].mass*wk;
        u	+= PG[p_ID].u*PG[p_ID].mass*wk;
        Z 	+= PG[p_ID].z*PG[p_ID].mass*wk;
        ne  += PG[p_ID].ne*PG[p_ID].mass*wk;
        nh 	+= PG[p_ID].nh*PG[p_ID].mass*wk;
        vx	+= PG[p_ID].vel[0]*PG[p_ID].mass*wk;
        vy	+= PG[p_ID].vel[1]*PG[p_ID].mass*wk;
        vz	+= PG[p_ID].vel[2]*PG[p_ID].mass*wk;
    }
    
    if (rho>0)
    {
        u  /= rho;
        Z  /= rho;
        ne /= rho;
        nh /= rho;
        vx /= rho;
        vy /= rho;
        vz /= rho;
        mu = 1.0/(0.25+0.76*(0.75+ne));
        T = 80.7668*u*mu;
    }
    else
    {
        ne=1.16;nh=1;T=1e6;
    }
    
    CELL[cell_ID].U->rho    = rho;
    CELL[cell_ID].U->u      = u;
    CELL[cell_ID].U->T      = T;
    CELL[cell_ID].U->z      = Z;
    CELL[cell_ID].U->ne     = ne;
    CELL[cell_ID].U->nh     = nh;
    CELL[cell_ID].U->vel[0] = vx;
    CELL[cell_ID].U->vel[1] = vy;
    CELL[cell_ID].U->vel[2] = vz;
    
    return;
}


void tree_conserve_mass(void)
{
    long i, cell_ID;
    double M_part, M_cell;
    
    M_part = M_cell = 0;
    for (cell_ID=0; cell_ID<TOTAL_NUMBER_OF_CELLS; cell_ID++)
    {
        if ((CELL[cell_ID].HAS_U_ALLOCATED==1) && (CELL[cell_ID].sub_cell_check==0))
        {
            for (i=0; i<CELL[cell_ID].N_particles_contained; i++)
                M_part += PG[CELL[cell_ID].particle_IDs[i]].mass;
            M_cell += CELL[cell_ID].U->rho*CELL[cell_ID].width*CELL[cell_ID].width*CELL[cell_ID].width;
        }
    }
    
    if ((M_part>0) && (M_cell>0))
    {
        for (cell_ID=0; cell_ID<TOTAL_NUMBER_OF_CELLS; cell_ID++)
        {
            if ((CELL[cell_ID].HAS_U_ALLOCATED==1) && (CELL[cell_ID].sub_cell_check==0))
                CELL[cell_ID].U->rho *= (M_part/M_cell);
        }
    }
    return;
}


long find_cell_from_cell(double pos[3], long cell_ID_0)
{
    // check if outside domain
    int j;
    for (j=0; j<3; j++)
        if (pos[j]<-MAX_DISTANCE_FROM0 || pos[j]>=MAX_DISTANCE_FROM0) return -1;
    
    // invalid cell index
    if ((cell_ID_0<0) || (cell_ID_0>=TOTAL_NUMBER_OF_CELLS))
        cell_ID_0 = 0;
    
    // check if already in cell
    int in_cell_check=1;
    for (j=0; j<3; j++) {
        if (pos[j] >= CELL[cell_ID_0].min_x[j] + CELL[cell_ID_0].width ||
            pos[j] < CELL[cell_ID_0].min_x[j]) in_cell_check = 0;
    }
    
    // find parent cell it belongs
    int n_x[3], n_sub_ID;
    long cell_ID;
    
    cell_ID = cell_ID_0;
    while (in_cell_check == 0) {
        cell_ID = CELL[cell_ID].parent_ID;
        if (cell_ID < 0) return -1;
        
        in_cell_check = 1;
        for (j=0; j<3; j++) {
            if (pos[j] >= CELL[cell_ID].min_x[j] + CELL[cell_ID].width ||
                pos[j] < CELL[cell_ID].min_x[j]) in_cell_check = 0;
        }
    }
    
    // find the finest-level cell
    while (CELL[cell_ID].sub_cell_check == 1) {
        for (j=0; j<3; j++) {
            n_x[j] = (int)((pos[j] - CELL[cell_ID].min_x[j])/(CELL[cell_ID].width/N_SUBCELL));
            if (n_x[j] < 0) n_x[j] = 0;
            if (n_x[j] > N_SUBCELL - 1) n_x[j] = N_SUBCELL - 1;
        }
        n_sub_ID = n_x[0]*N_SUBCELL_2 + n_x[1]*N_SUBCELL + n_x[2];
        cell_ID = CELL[cell_ID].sub_cell_IDs[n_sub_ID];
    }
    return cell_ID;
}


double maxstep_escape_cell(double pos[3], double n_hat[3], long cell_id)
{
    int j;
    double dr_i;
    double dr = 0.5*CELL[cell_id].width;
    double eps = 1e-10*MAX_DISTANCE_FROM0;
    
    for (j=0; j<3; j++)
    {
        if (n_hat[j]!=0)
        {
            dr_i = (CELL[cell_id].min_x[j]-pos[j])/n_hat[j];
            if ((dr_i>=0) && (dr_i<dr)) dr = dr_i;
            dr_i += (CELL[cell_id].width)/n_hat[j];
            if ((dr_i>=0) && (dr_i<dr)) dr = dr_i;
        }
    }
    return dr+eps;
}


void tree_statistics(void)
{
    TOTAL_NUMBER_OF_PARENTS = 0;
    TOTAL_NUMBER_OF_LEAVES = 0;
    
    long n_cell;
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
    {
        if (CELL[n_cell].sub_cell_check) TOTAL_NUMBER_OF_PARENTS++;
        else TOTAL_NUMBER_OF_LEAVES++;
    }
    
    LEAF_IDS = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(long));
    PARENT_IDS = malloc(TOTAL_NUMBER_OF_PARENTS*sizeof(long));
    
    long i=0, j=0;
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
    {
        if (CELL[n_cell].sub_cell_check) { PARENT_IDS[i]=n_cell; i++; }
        else { LEAF_IDS[j]=n_cell; j++; }
    }
}


void free_the_tree(void)
{
    long n_cell;
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
    {
        if (CELL[n_cell].sub_cell_check==1) free(CELL[n_cell].sub_cell_IDs);
        if (CELL[n_cell].HAS_pIDs_ALLOCATED==1) free(CELL[n_cell].particle_IDs);
        if (CELL[n_cell].HAS_U_ALLOCATED==1) free(CELL[n_cell].U);
        if (CELL[n_cell].HAS_dU_ALLOCATED==1) free(CELL[n_cell].dU);
    }
    free(CELL);
    free(PARENT_IDS);
    free(LEAF_IDS);
    return;
}
