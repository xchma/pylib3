// in allocate.c
void allocate_3d(void);
void set_particle_pointer(float*);
void free_memory_3d(void);
void allocate_gas(void);
void set_gas_particle(float*);
void free_memory_gas(void);

// in io.c
void write_tree_to_file(char*);
void read_tree_from_file(char*);

// in kernel.c
void set_sph_kernel(void);

// in ray.c
void initialize_the_ray(Ray_struct*);
void carry_ray_across_cell(Ray_struct*);
void integrate_ray_to_escape(Ray_struct*);

// in tree.c
void initialize_the_tree(void);
void build_cell(long);
void get_tree_localvals(long);
long find_cell_from_cell(double*,long);
double maxstep_escape_cell(double*,double*,long);
void tree_conserve_mass(void);
void tree_statistics(void);
void free_the_tree(void);
