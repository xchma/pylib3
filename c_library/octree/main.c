#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvar.h"
#include "proto.h"
#include "ngbtree3d.h"

void build_octree_from_particle(int N_gas, float* data, double* cen, char* fname,
                                double MAX_DISTANCE_FROM0_in, double TREE_MIN_SIZE_in,
                                int TREE_MAX_NPART_in)
{
    All.N_gas = (long)N_gas;
    int i; for (i=0;i<3;i++) CENTER[i]=cen[i];
    MAX_DISTANCE_FROM0 = MAX_DISTANCE_FROM0_in;
    TREE_MIN_SIZE = TREE_MIN_SIZE_in;
    TREE_MAX_NPART = TREE_MAX_NPART_in;
    
    // set up gas particle
    allocate_3d();
    set_particle_pointer(data);
    allocate_gas();
    set_gas_particle(data);
    
    // build ngbtree, needed for local values
    float dummy[3];
    ngb3d_treeallocate(All.N_gas, 10*All.N_gas);
    ngb3d_treebuild((float **)&P3d[1], All.N_gas, 0, dummy, dummy);
    
    // build octree
    set_sph_kernel();
    initialize_the_tree();
    
    // check mass in domain
    long n_cell; double mass=0;
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
    {
        if (CELL[n_cell].HAS_U_ALLOCATED==1)
            mass += CELL[n_cell].U->rho*pow(CELL[n_cell].width,3);
    }
    printf("Total mass in grid=%g\n", mass);
    
    // get tree statistics
    tree_statistics();
    
    // write octree to file
    write_tree_to_file(fname);
    
    // free memory
    free_memory_3d();
    free_memory_gas();
    ngb3d_treefree();
    free_the_tree();
    
    return;
}
