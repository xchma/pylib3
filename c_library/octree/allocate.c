#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvar.h"


void allocate_3d(void)
{
    long i, Nsize;
    Nsize = All.N_gas;
    
    if (Nsize>0)
    {
        if(!(P3d=malloc(Nsize*sizeof(struct particle_3d *))))
        {
            printf("failed to allocate memory. (A)\n");
            exit(0);
        }
        
        P3d--;   /* start with offset 1 */
        
        if(!(P3d[1]=malloc(Nsize*sizeof(struct particle_3d))))
        {
            printf("failed to allocate memory. (B)\n");
            exit(0);
        }
        
        for(i=2;i<=Nsize;i++)   /* initiliaze pointer table */
            P3d[i]=P3d[i-1]+1;
        
    }
}


void set_particle_pointer(float* Pos)
{
    long i;
    float *pos;
    
    for(i=1,pos=Pos;i<=All.N_gas;i++)
    {
        
        P3d[i]->Pos[0] = pos[0];
        P3d[i]->Pos[1] = pos[1];
        P3d[i]->Pos[2] = pos[2];
        
        pos+=12;
    }
}


void free_memory_3d(void)
{ 
    long Nsize = All.N_gas;
    
    if(Nsize>0)
    {
        free(P3d[1]);
        P3d++;
        free(P3d);
    }
}


void allocate_gas(void)
{
    if(!(PG=malloc(All.N_gas*sizeof(struct particle_gas))))
    {
        fprintf(stderr,"failed to allocate memory. (PG) \n");
        exit(0);
    }
}


void set_gas_particle(float* Pos)
{
    long i;
    float *pos;
    
    for(i=0,pos=Pos;i<All.N_gas;i++)
    {
        PG[i].pos[0] = pos[0];
        PG[i].pos[1] = pos[1];
        PG[i].pos[2] = pos[2];
        
        PG[i].vel[0] = pos[3];
        PG[i].vel[1] = pos[4];
        PG[i].vel[2] = pos[5];
        
        PG[i].mass = pos[6];
        PG[i].hsml = pos[7];
        PG[i].nh = pos[8];
        PG[i].ne = pos[9];
        PG[i].u = pos[10];
        PG[i].z = pos[11];
        
        pos+=12;
    }
    
}


void free_memory_gas(void)
{
    free(PG);
}
