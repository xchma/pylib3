#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvar.h"


void set_sph_kernel(void)
{
    long i;
    
    for (i=0;i<=KERNEL_TABLE+1;i++)
        KernelRad[i] = ((double)i)/KERNEL_TABLE;
    
    for (i=0;i<=KERNEL_TABLE;i++){
        
        if(KernelRad[i]<=0.5){
            Kernel[i] = 8/PI *(1-6*KernelRad[i]*KernelRad[i]*(1-KernelRad[i]));
        }else{
            Kernel[i] = 8/PI * 2*(1-KernelRad[i])*(1-KernelRad[i])*(1-KernelRad[i]);
        }
    }
    
    Kernel[KERNEL_TABLE+1] = 0;
    
}
