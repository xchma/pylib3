#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "allvar.h"
#include "proto.h"


void write_tree_to_file(char* fname)
{
    printf("Write tree to file %s\n", fname);
    
    hid_t file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;
    
    // write arributes
    hid_t hdf5_dataspace = H5Screate(H5S_SCALAR);
    hid_t hdf5_attribute = H5Acreate2(file_id, "Ncell", H5T_NATIVE_LONG, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_LONG, &TOTAL_NUMBER_OF_CELLS);
    
    hdf5_attribute = H5Acreate2(file_id, "Nparent", H5T_NATIVE_LONG, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_LONG, &TOTAL_NUMBER_OF_PARENTS);
    
    hdf5_attribute = H5Acreate2(file_id, "Nleaf", H5T_NATIVE_LONG, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_LONG, &TOTAL_NUMBER_OF_LEAVES);
    
    hdf5_attribute = H5Acreate2(file_id, "Boxsize", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &MAX_DISTANCE_FROM0);
    
    hdf5_attribute = H5Acreate2(file_id, "MinSize", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &TREE_MIN_SIZE);
    
    hdf5_attribute = H5Acreate2(file_id, "MaxNpart", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &TREE_MAX_NPART);
    
    status = H5LTset_attribute_double(file_id, "/", "Center", CENTER, 3);
    
    // write datasets
    long i, n_cell;
    int* buf_int; long* buf_long; double* buf_double;
    
    buf_double = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(double));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
        buf_double[n_cell] = CELL[n_cell].width;
    hsize_t dims1[1] = {TOTAL_NUMBER_OF_CELLS};
    status = H5LTmake_dataset_double(file_id, "width", 1, dims1, buf_double);
    free(buf_double); // width
    
    buf_double = malloc(TOTAL_NUMBER_OF_CELLS*3*sizeof(double));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
        for (i=0; i<3; i++) buf_double[3*n_cell+i] = CELL[n_cell].min_x[i];
    hsize_t dims2[2] = {TOTAL_NUMBER_OF_CELLS,3};
    status = H5LTmake_dataset_double(file_id, "min_x", 2, dims2, buf_double);
    free(buf_double); // min_x
    
    buf_long = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(long));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
        buf_long[n_cell] = CELL[n_cell].parent_ID;
    status = H5LTmake_dataset_long(file_id, "parent_ID", 1, dims1, buf_long);
    free(buf_long); // parent_id
    
    buf_int = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(int));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
        buf_int[n_cell] = CELL[n_cell].sub_cell_check;
    status = H5LTmake_dataset_int(file_id, "sub_cell_check", 1, dims1, buf_int);
    free(buf_int); // sub_cell_check
    
    buf_long = malloc(TOTAL_NUMBER_OF_PARENTS*N_SUBCELL_3*sizeof(long));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_PARENTS; n_cell++)
        for (i=0; i<N_SUBCELL_3; i++) buf_long[n_cell*N_SUBCELL_3+i] = CELL[PARENT_IDS[n_cell]].sub_cell_IDs[i];
    dims2[0] = TOTAL_NUMBER_OF_PARENTS; dims2[1] = N_SUBCELL_3;
    status = H5LTmake_dataset_long(file_id, "sub_cell_IDs", 2, dims2, buf_long);
    free(buf_long); // sub_cell_IDs
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*3*sizeof(double));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        for (i=0; i<3; i++) buf_double[n_cell*3+i] = CELL[LEAF_IDS[n_cell]].U->vel[i];
    dims2[0] = TOTAL_NUMBER_OF_LEAVES; dims2[1] = 3;
    status = H5LTmake_dataset_double(file_id, "vel", 2, dims2, buf_double);
    free(buf_double); // vel
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        buf_double[n_cell] = CELL[LEAF_IDS[n_cell]].U->rho;
    dims1[0] = TOTAL_NUMBER_OF_LEAVES;
    status = H5LTmake_dataset_double(file_id, "rho", 1, dims1, buf_double);
    free(buf_double); // rho
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        buf_double[n_cell] = CELL[LEAF_IDS[n_cell]].U->nh;
    dims1[0] = TOTAL_NUMBER_OF_LEAVES;
    status = H5LTmake_dataset_double(file_id, "nh", 1, dims1, buf_double);
    free(buf_double); // nh
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        buf_double[n_cell] = CELL[LEAF_IDS[n_cell]].U->z;
    status = H5LTmake_dataset_double(file_id, "z", 1, dims1, buf_double);
    free(buf_double); // z
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        buf_double[n_cell] = CELL[LEAF_IDS[n_cell]].U->T;
    status = H5LTmake_dataset_double(file_id, "T", 1, dims1, buf_double);
    free(buf_double); // T
    
    // finished
    H5Fclose(file_id);
}


void read_tree_from_file(char* fname)
{
    printf("Read octree from file %s\n", fname);
    
    hid_t file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    herr_t status;
    
    // get attributes
    status = H5LTget_attribute_long(file_id,"/","Ncell",&TOTAL_NUMBER_OF_CELLS);
    status = H5LTget_attribute_long(file_id,"/","Nparent",&TOTAL_NUMBER_OF_PARENTS);
    status = H5LTget_attribute_long(file_id,"/","Nleaf",&TOTAL_NUMBER_OF_LEAVES);
    status = H5LTget_attribute_double(file_id,"/","Boxsize",&MAX_DISTANCE_FROM0);
    //status = H5LTget_attribute_double(file_id,"/","MinSize",&TREE_MIN_SIZE);
    //status = H5LTget_attribute_int(file_id,"/","MaxNpart",&TREE_MAX_NPART);
    //status = H5LTget_attribute_double(file_id,"/","Center",CENTER);
    
    printf("Total number of cells=%ld\n",TOTAL_NUMBER_OF_CELLS);
    printf("Octree centered at %g, %g, %g\n", CENTER[0], CENTER[1], CENTER[2]);
    printf("Domain half size = %g\n", MAX_DISTANCE_FROM0);
    //printf("Minimum cell size allowed before splitting = %g\n", TREE_MIN_SIZE);
    //printf("Maximum number of particles allowed in cell = %d\n", TREE_MAX_NPART);
    
    // allocate memory for cells
    if(!(CELL=malloc(TOTAL_NUMBER_OF_CELLS*sizeof(struct CELL_STRUCT))))
    {
        fprintf(stderr, "Failed to allocate memory (CELL).\n");
        exit(0);
    }
    LEAF_IDS = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(long));
    PARENT_IDS = malloc(TOTAL_NUMBER_OF_PARENTS*sizeof(long));
    
    // get datasets
    long i, j, n_cell;
    int* buf_int; long* buf_long; double* buf_double;
    
    buf_int = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(int));
    status = H5LTread_dataset_int(file_id,"/sub_cell_check",buf_int);
    
    for (i=j=n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
    {
        CELL[n_cell].sub_cell_check = buf_int[n_cell];
        if (CELL[n_cell].sub_cell_check==1) {
            CELL[n_cell].sub_cell_IDs = malloc(N_SUBCELL_3*sizeof(long));
            PARENT_IDS[i] = n_cell; i++;
        } else {
            CELL[n_cell].U = malloc(sizeof(LOCALVAL));
            LEAF_IDS[j] = n_cell; j++;
        }
    }
    
    free(buf_int); // sub_cell_check
    
    buf_double = malloc(3*TOTAL_NUMBER_OF_CELLS*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/min_x",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
        for (i=0; i<3; i++) CELL[n_cell].min_x[i] = buf_double[3*n_cell+i];
    
    free(buf_double); // min_x
    
    buf_double = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/width",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
        CELL[n_cell].width = buf_double[n_cell];
    
    free(buf_double); // width
    
    buf_long = malloc(TOTAL_NUMBER_OF_CELLS*sizeof(long));
    status = H5LTread_dataset_long(file_id,"/parent_ID",buf_long);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_CELLS; n_cell++)
        CELL[n_cell].parent_ID = buf_long[n_cell];
    
    free(buf_long); // parent_ID
    
    buf_long = malloc(TOTAL_NUMBER_OF_PARENTS*N_SUBCELL_3*sizeof(long));
    status = H5LTread_dataset_long(file_id,"/sub_cell_IDs",buf_long);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_PARENTS; n_cell++)
        for (i=0; i<N_SUBCELL_3; i++) CELL[PARENT_IDS[n_cell]].sub_cell_IDs[i] = buf_long[N_SUBCELL_3*n_cell+i];
    
    free(buf_long); // sub_cell_IDs
    
    buf_double = malloc(3*TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/vel",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        for (i=0; i<3; i++) CELL[LEAF_IDS[n_cell]].U->vel[i] = buf_double[3*n_cell+i];
    
    free(buf_double); // vel
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/rho",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        CELL[LEAF_IDS[n_cell]].U->rho = buf_double[n_cell];
    
    free(buf_double); // rho
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/nh",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        CELL[LEAF_IDS[n_cell]].U->nh = buf_double[n_cell];
    
    free(buf_double); // nh
    
    /*
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/ne",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        CELL[LEAF_IDS[n_cell]].U->ne = buf_double[n_cell];
    
    free(buf_double); // ne
    */
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/z",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        CELL[LEAF_IDS[n_cell]].U->z = buf_double[n_cell];
    
    free(buf_double); // z
    
    buf_double = malloc(TOTAL_NUMBER_OF_LEAVES*sizeof(double));
    status = H5LTread_dataset_double(file_id,"/T",buf_double);
    
    for (n_cell=0; n_cell<TOTAL_NUMBER_OF_LEAVES; n_cell++)
        CELL[LEAF_IDS[n_cell]].U->T = buf_double[n_cell];
    
    free(buf_double); // T
    
    // finished
    H5Fclose(file_id);
}
    
    
