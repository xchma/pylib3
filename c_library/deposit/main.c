#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

double cubic_kernel(double u);

/*
 Notes:
 1. To ensure safety, it would be better to trim particles before calling these routines. Trimming on z-direction is not considered in 2D cases. It is intentionally designed such that it can be fully compatible with 2D problems.
 2. We must use double precision, because float precision can raise huge round error.
 */

/* number counts in 3D */
int count3d(int Npart, double* posx, double* posy, double* posz, double* left_edge, double* right_edge, int* dims, int* data){
    
    /* define boundaries */
    int Nx=dims[0],Ny=dims[1],Nz=dims[2];
    double xl=left_edge[0],xr=right_edge[0];
    double yl=left_edge[1],yr=right_edge[1];
    double zl=left_edge[2],zr=right_edge[2];
    double dx=(xr-xl)/Nx,dy=(yr-yl)/Ny,dz=(zr-zl)/Nz;
    double dxinv=1./dx,dyinv=1./dy,dzinv=1./dz;
    printf("Deposit particle:(xl|yl|zl)=(%.2f|%.2f|%.2f),(xr|yr|zr)=(%.2f|%.2f|%.2f),(Nx|Ny|Nz)=(%d|%d|%d)\n",xl,yl,zl,xr,yr,zr,Nx,Ny,Nz);
    
    /* claim variables */
    int n,ip,jp,kp,neff=0;
    double x,y,z;
    
    /* loop over particles */
    for(n=0;n<Npart;n++){
        
        x=posx[n]; y=posy[n]; z=posz[n];
        if((x<xl)||(x>xr)||(y<yl)||(y>yr)||(z<zl)||(z>zr)) continue;
        
        /* find the cell of current particle */
        ip=(int)((x-xl)*dxinv);
        jp=(int)((y-yl)*dyinv);
        kp=(int)((z-zl)*dzinv);
        
        if(neff%1000000==0){
            printf("neff=%d,n=%d,(x|y|z)=(%.2f|%.2f|%.2f),(i|j|k)=(%d|%d|%d)\n",neff,n,x,y,z,ip,jp,kp);
        }
        
        neff++;
        *(data+ip*(Ny*Nz)+jp*Nz+kp)+=1;
        
    }
    
    printf("Finished. Total %d particles in the domain.\n",neff);
    
    return 1;
    
}


/* number counts in 2D */
int count2d(int Npart, double* posx, double* posy, double* left_edge, double* right_edge, int* dims, int* data){
    
    /* define boundaries */
    int Nx=dims[0],Ny=dims[1];
    double xl=left_edge[0],xr=right_edge[0];
    double yl=left_edge[1],yr=right_edge[1];
    double dx=(xr-xl)/Nx,dy=(yr-yl)/Ny;
    double dxinv=1./dx,dyinv=1./dy;
    printf("Deposit particle:(xl|yl)=(%.2f|%.2f),(xr|yr)=(%.2f|%.2f),(Nx|Ny)=(%d|%d)\n",xl,yl,xr,yr,Nx,Ny);
    
    /* claim variables */
    int n,ip,jp,neff=0;
    double x,y;
    
    /* loop over particles */
    for(n=0;n<Npart;n++){
        
        x=posx[n]; y=posy[n];
        if((x<xl)||(x>xr)||(y<yl)||(y>yr)) continue;
        
        /* find the cell of current particle */
        ip=(int)((x-xl)*dxinv);
        jp=(int)((y-yl)*dyinv);
        
        if(neff%1000000==0){
            printf("neff=%d,n=%d,(x|y)=(%.2f|%.2f),(i|j)=(%d|%d)\n",neff,n,x,y,ip,jp);
        }
        
        neff++;
        *(data+ip*Ny+jp)+=1;
        
    }
    
    printf("Finished. Total %d particles in the domain.\n",neff);
    
    return 1;
    
}


/* simple deposit in 3D */
int simple3d(int Npart, double* posx, double* posy, double* posz, double* field, double* left_edge, double* right_edge, int* dims, double* data){
    
    /* define boundaries */
    int Nx=dims[0],Ny=dims[1],Nz=dims[2];
    double xl=left_edge[0],xr=right_edge[0];
    double yl=left_edge[1],yr=right_edge[1];
    double zl=left_edge[2],zr=right_edge[2];
    double dx=(xr-xl)/Nx,dy=(yr-yl)/Ny,dz=(zr-zl)/Nz;
    double dxinv=1./dx,dyinv=1./dy,dzinv=1./dz;
    printf("Deposit particle:(xl|yl|zl)=(%.2f|%.2f|%.2f),(xr|yr|zr)=(%.2f|%.2f|%.2f),(Nx|Ny|Nz)=(%d|%d|%d)\n",xl,yl,zl,xr,yr,zr,Nx,Ny,Nz);
    
    /* claim variables*/
    int n,ip,jp,kp,neff=0;
    double x,y,z,m,mass=0;
    
    /* loop over particles */
    for(n=0;n<Npart;n++){
        
        x=posx[n]; y=posy[n]; z=posz[n]; m=field[n];
        if((x<xl)||(x>xr)||(y<yl)||(y>yr)||(z<zl)||(z>zr)) continue;
        
        /* find the cell of current particle */
        ip=(int)((x-xl)*dxinv);
        jp=(int)((y-yl)*dyinv);
        kp=(int)((z-zl)*dzinv);
        
        if(neff%1000000==0){
            printf("neff=%d,n=%d,(x|y|z|m)=(%.2f|%.2f|%.2f|%.2e),(i|j|k)=(%d|%d|%d)\n",neff,n,x,y,z,m,ip,jp,kp);
        }
        
        neff+=1; mass+=m;
        *(data+ip*(Ny*Nz)+jp*Nz+kp)+=m;
        
    }
    
    printf("Finished. Total %d particles of mass %g in the domain.\n",neff,mass);
    
    return 1;
}


/* simple deposit in 2D */
int simple2d(int Npart, double* posx, double* posy, double* field, double* left_edge, double* right_edge, int* dims, double* data){
    
    /* define boundaries */
    int Nx=dims[0],Ny=dims[1];
    double xl=left_edge[0],xr=right_edge[0];
    double yl=left_edge[1],yr=right_edge[1];
    double dx=(xr-xl)/Nx,dy=(yr-yl)/Ny;
    double dxinv=1./dx,dyinv=1./dy;
    printf("Deposit particle:(xl|yl)=(%.2f|%.2f),(xr|yr)=(%.2f|%.2f),(Nx|Ny)=(%d|%d)\n",xl,yl,xr,yr,Nx,Ny);
    
    /* claim variables*/
    int n,ip,jp,neff=0;
    double x,y,m,mass=0;
    
    /* loop over particles */
    for(n=0;n<Npart;n++){
        
        x=posx[n]; y=posy[n]; m=field[n];
        if((x<xl)||(x>xr)||(y<yl)||(y>yr)) continue;
        
        /* find the cell of current particle */
        ip=(int)((x-xl)*dxinv);
        jp=(int)((y-yl)*dyinv);
        
        if(neff%1000000==0){
            printf("neff=%d,n=%d,(x|y|m)=(%.2f|%.2f|%.2e),(i|j)=(%d|%d)\n",neff,n,x,y,m,ip,jp);
        }
        
        neff+=1; mass+=m;
        *(data+ip*Ny+jp)+=m;
        
    }
    
    printf("Finished. Total %d particles of mass %g in the domain.\n",neff,mass);
    
    return 1;
}


/* smoothed deposit in 3D using cubic kernel */
int smooth3d(int Npart, double* posx, double* posy, double* posz, double* hsml, double* field, double* left_edge, double* right_edge, int* dims, double* data, double* temp){
    
    /* define boundaries */
    int Nx=dims[0],Ny=dims[1],Nz=dims[2];
    double xl=left_edge[0],xr=right_edge[0];
    double yl=left_edge[1],yr=right_edge[1];
    double zl=left_edge[2],zr=right_edge[2];
    double dx=(xr-xl)/Nx,dy=(yr-yl)/Ny,dz=(zr-zl)/Nz;
    double dxinv=1./dx,dyinv=1./dy,dzinv=1./dz;
    printf("Deposit particle:(xl|yl|zl)=(%.2f|%.2f|%.2f),(xr|yr|zr)=(%.2f|%.2f|%.2f),(Nx|Ny|Nz)=(%d|%d|%d)\n",xl,yl,zl,xr,yr,zr,Nx,Ny,Nz);
    
    /* claim variables*/
    int n,i,j,k,ip,jp,kp,neff=0;
    double x,y,z,h,m,u,wt,wt_sum=0,mass=0;
    int imin,imax,jmin,jmax,kmin,kmax;
    double xcell,ycell,zcell;
    
    /* loop over particles */
    for(n=0;n<Npart;n++){
        
        x=posx[n]; y=posy[n]; z=posz[n]; h=hsml[n]; m=field[n];
        if((x<xl)||(x>xr)||(y<yl)||(y>yr)||(z<zl)||(z>zr)) continue;
        
        /* find the cell of current particle */
        ip=(int)((x-xl)*dxinv);
        jp=(int)((y-yl)*dyinv);
        kp=(int)((z-zl)*dzinv);
        imin=(int)((x-h-xl)*dxinv); if(imin<0) {imin=0;}
        imax=(int)((x+h-xl)*dxinv); if(imax>=Nx) {imax=Nx-1;}
        jmin=(int)((y-h-yl)*dyinv); if(jmin<0) {jmin=0;}
        jmax=(int)((y+h-yl)*dyinv); if(jmax>=Ny) {jmax=Ny-1;}
        kmin=(int)((z-h-zl)*dzinv); if(kmin<0) {kmin=0;}
        kmax=(int)((z+h-zl)*dzinv); if(kmax>=Nz) {kmax=Nz-1;}
        
        if(neff%1000000==0){
            printf("neff=%d,n=%d,(x|y|z|m)=(%.2f|%.2f|%.2f|%.2e),(i|j|k)=(%d|%d|%d)\n",neff,n,x,y,z,m,ip,jp,kp);
        }
        neff+=1; mass+=m;
        
        /* loop over to calculate the weights */
        wt_sum=0;
        for(i=imin;i<=imax;i++){
            for(j=jmin;j<=jmax;j++){
                for(k=kmin;k<=kmax;k++){
                    xcell=xl+(i+0.5)*dx; ycell=yl+(j+0.5)*dy; zcell=zl+(k+0.5)*dz;
                    u=sqrt((x-xcell)*(x-xcell)+(y-ycell)*(y-ycell)+(z-zcell)*(z-zcell))/h;
                    wt=cubic_kernel(u);
                    *(temp+i*(Ny*Nz)+j*Nz+k)=wt;
                    wt_sum+=wt;
                }
            }
        }
        
        /* now, deposit mass to the cell */
        if(wt_sum>0){
            /* if total weights is not zero, loop over the cells again */
            for(i=imin;i<=imax;i++){
                for(j=jmin;j<=jmax;j++){
                    for(k=kmin;k<=kmax;k++){
                        wt=*(temp+i*(Ny*Nz)+j*Nz+k);
                        *(data+i*(Ny*Nz)+j*Nz+k)+=m*wt/wt_sum;
                    }
                }
            }
        }
        else{
            /* if total weight is zero, deposit it to the cell only */
            *(data+ip*(Ny*Nz)+jp*Nz+kp)+=m;
        }
        
    }
    
    printf("Finished. Total %d particles of mass %g in the domain.\n",neff,mass);

    return 1;
}


/* smoothed deposit in 2D using cubic kernel */
int smooth2d(int Npart, double* posx, double* posy, double* hsml, double* field, double* left_edge, double* right_edge, int* dims, double* data, double* temp){
    
    /* define boundaries */
    int Nx=dims[0],Ny=dims[1];
    double xl=left_edge[0],xr=right_edge[0];
    double yl=left_edge[1],yr=right_edge[1];
    double dx=(xr-xl)/Nx,dy=(yr-yl)/Ny;
    double dxinv=1./dx,dyinv=1./dy;
    printf("Deposit particle:(xl|yl)=(%.2f|%.2f),(xr|yr)=(%.2f|%.2f),(Nx|Ny)=(%d|%d)\n",xl,yl,xr,yr,Nx,Ny);
    
    /* claim variables*/
    int n,i,j,ip,jp,neff=0;
    double x,y,h,m,u,wt,wt_sum=0,mass=0;
    int imin,imax,jmin,jmax;
    double xcell,ycell;
    
    /* loop over particles */
    for(n=0;n<Npart;n++){
        
        x=posx[n]; y=posy[n]; h=hsml[n]; m=field[n];
        if((x<xl)||(x>xr)||(y<yl)||(y>yr)) continue;
        
        /* find the cell of current particle */
        ip=(int)((x-xl)*dxinv);
        jp=(int)((y-yl)*dyinv);
        imin=(int)((x-h-xl)*dxinv); if(imin<0) {imin=0;}
        imax=(int)((x+h-xl)*dxinv); if(imax>=Nx) {imax=Nx-1;}
        jmin=(int)((y-h-yl)*dyinv); if(jmin<0) {jmin=0;}
        jmax=(int)((y+h-yl)*dyinv); if(jmax>=Ny) {jmax=Ny-1;}
        
        if(neff%1000000==0){
            printf("neff=%d,n=%d,(x|y|m)=(%.2f|%.2f|%.2e),(i|j)=(%d|%d)\n",neff,n,x,y,m,ip,jp);
        }
        neff+=1; mass+=m;
        
        /* loop over to calculate the weights */
        wt_sum=0;
        for(i=imin;i<=imax;i++){
            for(j=jmin;j<=jmax;j++){
                xcell=xl+(i+0.5)*dx; ycell=yl+(j+0.5)*dy;
                u=sqrt((x-xcell)*(x-xcell)+(y-ycell)*(y-ycell))/h;
                wt=cubic_kernel(u);
                *(temp+i*Ny+j)=wt;
                wt_sum+=wt;
            }
        }
        
        /* now, deposit mass to the cell */
        if(wt_sum>0){
            /* if total weights is not zero, loop over the cells again */
            for(i=imin;i<=imax;i++){
                for(j=jmin;j<=jmax;j++){
                    wt=*(temp+i*Ny+j);
                    *(data+i*Ny+j)+=m*wt/wt_sum;
                }
            }
        }
        else{
            /* if total weight is zero, deposit it to the cell only */
            *(data+ip*Ny+jp)+=m;
        }
        
    }
    
    printf("Finished. Total %d particles of mass %g in the domain.\n",neff,mass);
    
    return 1;
}


/* we use cubic kernel here, not normalized */
double cubic_kernel(double u){
    
    double wt = 0.0;
    if((u>=0)&&(u<=0.5)) wt = 1.0+6.0*(u-1.0)*u*u;
    if((u>0.5)&&(u<1)) wt = 2.0*(1.0 - u)*(1.0 - u)*(1.0 - u);
    wt *= 2.5464791;
    
    return wt;
    
}
