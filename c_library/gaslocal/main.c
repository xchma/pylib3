#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "proto.h"
#include "ngbtree3d.h"
#include "kernel.h"

void allocate_3d(int Nsize, int Nfield);
void set_particle_pointer(int Nsize, float* pos, double* mass, double* hsml, int Nfield, double* data);
void free_memory_3d(int Nsize);

struct particle_3d 
{
    float Pos[3];
    double Hsml;
    double Mass;
    double* Data;
} **P3d;


// revised call for python calling //
//int stellarhsml(int argc,void *argv[])
int gaslocal(int N_in, float* pos, double* mass, double* hsml,
             int N_field, double* data, int N_out, float* pos_out,
             int DesNgb, float Hmax, double* rho_out, double* data_out)
{
    float h_guess, h2, xyz[3], dummy[3], h_guess_0;
    double h, hinv, hinv3, rho, wt;
    double dx, dy, dz, r, rhinv;
    int i, j, k, l, ngbfound;
    allocate_3d(N_in, N_field);
    float *r2list;
    int *ngblist;
    
    printf("N=%d\n",N_in);
    printf("Hmax=%g\n",Hmax);
    printf("DesNgb=%d\n",DesNgb);

    ngb3d_treeallocate(N_in, 2*N_in);
    set_particle_pointer(N_in, pos, mass, hsml, N_field, data);
    ngb3d_treebuild((float **)&P3d[1], N_in, 0, dummy, dummy);
    h_guess = Hmax/150.0e0; h_guess_0=h_guess;
    
  // unlike stellar hmsl function, we loop over the new points here
    for(i=0;i<N_out;i++)
    {
        // new coordinates where we want the velocity
        xyz[0]=pos_out[3*i]; xyz[1]=pos_out[3*i+1]; xyz[2]=pos_out[3*i+2];
        h2 = ngb3d_treefind( xyz, DesNgb ,1.04*h_guess, &ngblist, &r2list, Hmax, &ngbfound);

        rho = wt = 0.0;
        for (l=0; l<N_field; l++)
            data_out[N_field*i+l] = 0;
        // loop over all neighbours
        for (j=0; j<ngbfound; j++)
        {
            k = ngblist[j];
            dx = P3d[k+1]->Pos[0] - xyz[0];
            dy = P3d[k+1]->Pos[1] - xyz[1];
            dz = P3d[k+1]->Pos[2] - xyz[2];
            r = sqrt(dx*dx + dy*dy + dz*dz);
            h = sqrt(h2); hinv = 1.0/h;
            //h = P3d[k+1]->Hsml; hinv = 1.0/h;
            hinv3 = hinv*hinv*hinv; rhinv = r*hinv;
            wt = kernel_main(rhinv, hinv3); //printf("wt=%g, h=%g, mass=%g\n", wt, h, P3d[k+1]->Mass);
            rho += wt * P3d[k+1]->Mass;
            for (l=0; l<N_field; l++)
                data_out[N_field*i+l] += wt * P3d[k+1]->Mass * P3d[k+1]->Data[l];
        }
        rho_out[i] = rho;
        if (rho>0)
            for (l=0; l<N_field; l++) data_out[N_field*i+l] /= rho;
        
        if(!(i%10000))
        {
            printf("i=%d hmax=%g h_guess=%g h=%g xyz=%g|%g|%g ngb=%d \n", i, Hmax, h_guess, sqrt(h2), xyz[0], xyz[1], xyz[2], ngbfound);
            fflush(stdout);
        }
        h_guess = sqrt(h2); // use this value for next guess, should speed things up //
        //if (h_guess>10.*h_guess_0) h_guess=2.*h_guess_0;
    }
    
    ngb3d_treefree();
    free_memory_3d(N_in);
    printf("done\n");
    return 0;
}



void set_particle_pointer(int Nsize, float* pos, double* mass,
                          double* hsml, int Nfield, double* data)
{
    int i, j;
    for (i=1; i<=Nsize; i++)
    {
        P3d[i]->Pos[0] = pos[3*i-3];
        P3d[i]->Pos[1] = pos[3*i-2];
        P3d[i]->Pos[2] = pos[3*i-1];
        P3d[i]->Mass = mass[i-1];
        P3d[i]->Hsml = hsml[i-1];
        for (j=0; j<Nfield; j++)
            P3d[i]->Data[j] = data[Nfield*(i-1)+j];
    }
}


void allocate_3d(int Nsize, int Nfield)
{
    printf("allocating memory...\n");
    int i;
    if (Nsize>0)
    {
        if(!(P3d=malloc(Nsize*sizeof(struct particle_3d *))))
        {
            printf("failed to allocate memory. (A)\n");
            exit(0);
        }
        
        P3d--;   /* start with offset 1 */
        if(!(P3d[1]=malloc(Nsize*sizeof(struct particle_3d))))
        {
            printf("failed to allocate memory. (B)\n");
            exit(0);
        }
        for (i=2; i<=Nsize; i++)   /* initiliaze pointer table */
            P3d[i] = P3d[i-1] + 1;
        
        for (i=1; i<=Nsize; i++)
        if (!(P3d[i]->Data = malloc(Nfield*sizeof(double))))
        {
            printf("failed to allocate memory. (C%d)\n", i);
        }
    }
    printf("allocating memory...done\n");
}


void free_memory_3d(int Nsize)
{
    if (Nsize>0)
    {
        int i;
        for (i=1; i<=Nsize; i++)
            free(P3d[i]->Data);
        free(P3d[1]);
        P3d++;
        free(P3d);
    }
}
