/* this file contains the definitions for the kernel functions */
/*
 * This file was originally part of the GADGET3 code developed by
 * Volker Springel (volker.springel@h-its.org). The code has been modified
 * substantially by Phil Hopkins (phopkins@caltech.edu) for GIZMO (added the 
 * calls to this for other uses of the kernel in-code in areas like gravity;
 * also added new kernels here)
 */


#define  KERNEL_NORM  2.5464791                 /*!< For 3D-normalized kernel */

static inline float kernel_main(float u, float hinv3)
{

  float wt = 0.0;
  if ((u>=0)&&(u<=0.5)) wt = 1.0 + 6.0 * (u - 1.0) * u * u;
  if ((u>0.5)&&(u<1)) wt = 2.0 * (1.0 - u) * (1.0 - u) * (1.0 - u);

  wt *= KERNEL_NORM * hinv3;

  return wt;
}
