#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int match(int *a, int *b, int alen, int blen, int *suba, int *subb){
    int i,j;
    i=0; j=0;
    while ((i<alen) && (j<blen)) {
        if (a[i]>b[j]) {
            subb[j]=0; j++;
        }
        else if (a[i]<b[j]) {
            suba[i]=0; i++;
        }
        else {
            suba[i]=1; subb[j]=1; i++; j++;
        }
    }
    return 1;
}
