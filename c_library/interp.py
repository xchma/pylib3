import numpy as np
import ctypes
import os


def interp1d(x, v, xo):

    # convert data type
    x_copy = np.array(x, dtype='float64', copy=True)
    v_copy = np.array(v, dtype='float64', copy=True)
    xo_copy = np.array(xo, dtype='float64', copy=True)
    Nx_ctype = ctypes.c_int(x_copy.size)
    No_ctype = ctypes.c_int(xo_copy.size)
    x_ctype = x_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    v_ctype = v_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    xo_ctype = xo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    vo_ctype = (ctypes.c_double*xo_copy.size)()
    
    # call the routine
    exec_call = os.environ['PYLIB'] + "/c_library/interp/interp.so"
    routine = ctypes.cdll[exec_call]
    routine.interp1d(x_ctype, Nx_ctype, v_ctype, xo_ctype, No_ctype, vo_ctype)
    vo = np.ctypeslib.as_array(vo_ctype)

    return vo[0] if vo.size==1 else vo


def interp2d(x, y, v, xo, yo):

    # convert data type
    x_copy = np.array(x, dtype='float64', copy=True)
    y_copy = np.array(y, dtype='float64', copy=True)
    v_copy = np.array(v, dtype='float64', copy=True)
    xo_copy = np.array(xo, dtype='float64', copy=True)
    yo_copy = np.array(yo, dtype='float64', copy=True)
    Nx_ctype = ctypes.c_int(x_copy.size)
    Ny_ctype = ctypes.c_int(y_copy.size)
    No_ctype = ctypes.c_int(xo_copy.size)
    x_ctype = x_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    y_ctype = y_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    v_ctype = v_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    xo_ctype = xo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    yo_ctype = yo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    vo_ctype = (ctypes.c_double*xo_copy.size)()
    
    # call the routine
    exec_call = os.environ['PYLIB'] + "/c_library/interp/interp.so"
    routine = ctypes.cdll[exec_call]
    routine.interp2d(x_ctype, Nx_ctype, y_ctype, Ny_ctype, v_ctype, xo_ctype, yo_ctype, No_ctype, vo_ctype)
    vo = np.ctypeslib.as_array(vo_ctype)
    
    return vo[0] if vo.size==1 else vo


def interp3d(x, y, z, v, xo, yo, zo):

    # convert data type
    x_copy = np.array(x, dtype='float64', copy=True)
    y_copy = np.array(y, dtype='float64', copy=True)
    z_copy = np.array(z, dtype='float64', copy=True)
    v_copy = np.array(v, dtype='float64', copy=True)
    xo_copy = np.array(xo, dtype='float64', copy=True)
    yo_copy = np.array(yo, dtype='float64', copy=True)
    zo_copy = np.array(zo, dtype='float64', copy=True)
    Nx_ctype = ctypes.c_int(x_copy.size)
    Ny_ctype = ctypes.c_int(y_copy.size)
    Nz_ctype = ctypes.c_int(z_copy.size)
    No_ctype = ctypes.c_int(xo_copy.size)
    x_ctype = x_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    y_ctype = y_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    z_ctype = z_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    v_ctype = v_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    xo_ctype = xo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    yo_ctype = yo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    zo_ctype = zo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    vo_ctype = (ctypes.c_double*xo_copy.size)()
    
    # call the routine
    exec_call = os.environ['PYLIB'] + "/c_library/interp/interp.so"
    routine = ctypes.cdll[exec_call]
    routine.interp3d(x_ctype, Nx_ctype, y_ctype, Ny_ctype, z_ctype, Nz_ctype, v_ctype, xo_ctype, yo_ctype, zo_ctype, No_ctype, vo_ctype)
    vo = np.ctypeslib.as_array(vo_ctype)
    
    return vo[0] if vo.size==1 else vo


def interp4d(x, y, z, w, v, xo, yo, zo, wo):

    # convert data type
    x_copy = np.array(x, dtype='float64', copy=True)
    y_copy = np.array(y, dtype='float64', copy=True)
    z_copy = np.array(z, dtype='float64', copy=True)
    w_copy = np.array(w, dtype='float64', copy=True)
    v_copy = np.array(v, dtype='float64', copy=True)
    xo_copy = np.array(xo, dtype='float64', copy=True)
    yo_copy = np.array(yo, dtype='float64', copy=True)
    zo_copy = np.array(zo, dtype='float64', copy=True)
    wo_copy = np.array(wo, dtype='float64', copy=True)
    Nx_ctype = ctypes.c_int(x_copy.size)
    Ny_ctype = ctypes.c_int(y_copy.size)
    Nz_ctype = ctypes.c_int(z_copy.size)
    Nw_ctype = ctypes.c_int(w_copy.size)
    No_ctype = ctypes.c_int(xo_copy.size)
    x_ctype = x_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    y_ctype = y_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    z_ctype = z_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    w_ctype = w_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    v_ctype = v_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    xo_ctype = xo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    yo_ctype = yo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    zo_ctype = zo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    wo_ctype = wo_copy.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    vo_ctype = (ctypes.c_double*xo_copy.size)()
    
    # call the routine
    exec_call = os.environ['PYLIB'] + "/c_library/interp/interp.so"
    routine = ctypes.cdll[exec_call]
    routine.interp4d(x_ctype, Nx_ctype, y_ctype, Ny_ctype, z_ctype, Nz_ctype, w_ctype, Nw_ctype, v_ctype, xo_ctype, yo_ctype, zo_ctype, wo_ctype, No_ctype, vo_ctype)
    vo = np.ctypeslib.as_array(vo_ctype)
    
    return vo[0] if vo.size==1 else vo
