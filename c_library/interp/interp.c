#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


double single1d(double* x, int Nx, double* v, double xo)
{
    int il=0;
    double xl,xr,vl,vr,Cl,Cr;
    
    // upper bound
    double xmax = x[Nx-1] - 1e-5*(x[Nx-1]-x[Nx-2]);
    if (xo<x[0]) xo=x[0];
    if (xo>xmax) xo=xmax;
    while (x[il+1]<=xo) il++;
    
    xl=x[il]; xr=x[il+1];
    vl=v[il]; vr=v[il+1];
    Cl = (xo-xl)/(xr-xl);
    Cr = (xr-xo)/(xr-xl);
    
    return Cr*vl + Cl*vr;
}


double single2d(double* x, int Nx, double* y, int Ny, double* v, double xo, double yo)
{
    int il=0;
    double xl,xr,vl,vr,Cl,Cr;
    
    // upper bound
    double xmax = x[Nx-1] - 1e-5*(x[Nx-1]-x[Nx-2]);
    if (xo<x[0]) xo=x[0];
    if (xo>xmax) xo=xmax;
    while (x[il+1]<=xo) il++;
    
    xl=x[il]; xr=x[il+1];
    Cl = (xo-xl)/(xr-xl);
    Cr = (xr-xo)/(xr-xl);
    vl = single1d(y, Ny, v+il*Ny, yo);
    vr = single1d(y, Ny, v+(il+1)*Ny, yo);
    
    return Cr*vl + Cl*vr;
}


double single3d(double* x, int Nx, double* y, int Ny, double* z, int Nz, double* v, double xo, double yo, double zo)
{
    int il=0;
    double xl,xr,vl,vr,Cl,Cr;
    
    // upper bound
    double xmax = x[Nx-1] - 1e-5*(x[Nx-1]-x[Nx-2]);
    if (xo<x[0]) xo=x[0];
    if (xo>xmax) xo=xmax;
    while (x[il+1]<=xo) il++;
    
    xl=x[il]; xr=x[il+1];
    Cl = (xo-xl)/(xr-xl);
    Cr = (xr-xo)/(xr-xl);
    vl = single2d(y, Ny, z, Nz, v+il*(Ny*Nz), yo, zo);
    vr = single2d(y, Ny, z, Nz, v+(il+1)*(Ny*Nz), yo, zo);
    
    return Cr*vl + Cl*vr;
}


double single4d(double* x, int Nx, double* y, int Ny, double* z, int Nz, double* w, int Nw, double* v, double xo, double yo, double zo, double wo)
{
    int il=0;
    double xl,xr,vl,vr,Cl,Cr;
    
    // upper bound
    double xmax = x[Nx-1] - 1e-5*(x[Nx-1]-x[Nx-2]);
    if (xo<x[0]) xo=x[0];
    if (xo>xmax) xo=xmax;
    while (x[il+1]<=xo) il++;
    
    xl=x[il]; xr=x[il+1];
    Cl = (xo-xl)/(xr-xl);
    Cr = (xr-xo)/(xr-xl);
    vl = single3d(y, Ny, z, Nz, w, Nw, v+il*(Ny*Nz*Nw), yo, zo, wo);
    vr = single3d(y, Ny, z, Nz, w, Nw, v+(il+1)*(Ny*Nz*Nw), yo, zo, wo);
    
    return Cr*vl + Cl*vr;
}


void interp1d(double* x, int Nx, double* v, double* xo, int No, double* vo)
{
    int i; for (i=0;i<No;i++) vo[i]=single1d(x,Nx,v,xo[i]);
}


void interp2d(double* x, int Nx, double* y, int Ny, double* v, double* xo, double* yo, int No, double* vo)
{
    int i; for (i=0;i<No;i++) vo[i]=single2d(x,Nx,y,Ny,v,xo[i],yo[i]);
}


void interp3d(double* x, int Nx, double* y, int Ny, double* z, int Nz, double* v, double* xo, double* yo, double* zo, int No, double* vo)
{
    int i; for (i=0;i<No;i++) vo[i]=single3d(x,Nx,y,Ny,z,Nz,v,xo[i],yo[i],zo[i]);
}


void interp4d(double* x, int Nx, double* y, int Ny, double* z, int Nz, double* w, int Nw, double* v, double* xo, double* yo, double* zo, double* wo, int No, double* vo)
{
    int i; for (i=0;i<No;i++) vo[i]=single4d(x,Nx,y,Ny,z,Nz,w,Nw,v,xo[i],yo[i],zo[i],wo[i]);
}
