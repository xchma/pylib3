import numpy as np
import ctypes, os

def local_gas_quantities(pos, mass, hsml, data, pos_out, DesNgb=32, Hmax=100.):
    
    # convert data type
    pos = np.array(pos, ndmin=2, dtype='float32')
    pos_out = np.array(pos_out, ndmin=2, dtype='float32')
    N_in, N_out = np.int32(len(pos)), np.int32(len(pos_out))
    N_in_ctypes, N_out_ctypes = ctypes.c_int(N_in), ctypes.c_int(N_out)
    pos_ctypes = pos.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
    pos_out_ctypes = pos_out.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
    
    data = np.array(data, ndmin=2, dtype='float64')
    N_field = np.int32(data.shape[1])
    N_field_ctypes = ctypes.c_int(N_field)
    data_ctypes = data.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    data_out_format = ctypes.c_double*N_field*N_out
    data_out_ctypes = data_out_format()
    rho_out_format = ctypes.c_double*N_out
    rho_out_ctypes = rho_out_format()
    
    mass = np.array(mass, dtype='float64')
    mass_ctypes = mass.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    hsml = np.array(hsml, dtype='float64')
    hsml_ctypes = hsml.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
    DesNgb, Hmax = np.int32(DesNgb), np.float32(Hmax)
    DesNgb_ctypes = ctypes.c_int(DesNgb)
    Hmax_ctypes = ctypes.c_float(Hmax)
    
    # call the function
    exec_call = os.environ['PYLIB'] + '/c_library/gaslocal/gaslocal.so'
    routine = ctypes.cdll[exec_call]
    
    routine.gaslocal(N_in_ctypes, pos_ctypes, mass_ctypes, hsml_ctypes, N_field_ctypes, data_ctypes, N_out_ctypes, pos_out_ctypes, DesNgb_ctypes, Hmax_ctypes, rho_out_ctypes, data_out_ctypes)

    rho_out = np.ctypeslib.as_array(rho_out_ctypes)
    data_out = np.ctypeslib.as_array(data_out_ctypes)
    
    return rho_out, data_out
