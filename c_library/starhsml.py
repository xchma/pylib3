import numpy as np
import ctypes
import os

def get_particle_hsml(x, y, z, DesNgb=32, Hmax=0):
    
    N = len(x) # number of particles
    if (Hmax==0):
        dx = np.max(x) - np.min(x)
        dy = np.max(y) - np.min(y)
        dz = np.max(z) - np.min(z)
        Hmax = 5*max(dx,dy,dz)/N**(1/3)
    
    # load the routine to call
    exec_call = os.environ['PYLIB'] + '/c_library/starhsml/starhsml.so'
    h_routine = ctypes.cdll[exec_call]

    # convert data types
    x_float = np.array(x, dtype='float32')
    y_float = np.array(y, dtype='float32')
    z_float = np.array(z, dtype='float32')
    x_ctypes = x_float.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
    y_ctypes = y_float.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
    z_ctypes = z_float.ctypes.data_as(ctypes.POINTER(ctypes.c_float))
    h_ctypes = (ctypes.c_float*N)()

    # call the routine
    h_routine.stellarhsml(ctypes.c_int(N), x_ctypes, y_ctypes, z_ctypes, ctypes.c_int(DesNgb), ctypes.c_float(Hmax), h_ctypes)
    h = np.ctypeslib.as_array(h_ctypes)
    
    return h
