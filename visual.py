import numpy as np
import os

# find appropriate scale bar and label
def find_scale_bar(L):

    if (L>=10000):
        bar = 1000.; label = '1 Mpc'
    elif (L>=1000):
        bar = 100.; label = '100 kpc'
    elif (L>=500):
        bar = 50.; label = '50 kpc'
    elif (L>=200):
        bar = 20.; label = '20 kpc'
    elif (L>=100):
        bar = 10.; label = '10 kpc'
    elif (L>=50):
        bar = 5.; label = '5 kpc'
    elif (L>=20):
        bar = 2.; label = '2 kpc'
    elif (L>=10):
        bar = 1.; label = '1 kpc'
    elif (L>=5):
        bar = 0.5; label = '500 pc'
    elif (L>=2):
        bar = 0.2; label = '200 pc'
    elif (L>=1):
        bar = 0.1; label = '100 pc'
    elif (L>0.5):
        bar = 0.05; label = '50 pc'
    elif (L>0.2):
        bar = 0.02; label = '20 pc'
    elif (L>0.1):
        bar = 0.01; label = '10 pc'
    elif (L>0.05):
        bar = 0.005; label = '5 pc'
    elif (L>0.02):
        bar = 0.002; label = '2 pc'
    elif (L>0.01):
        bar = 0.001; label = '1 pc'
    else:
        bar = 0.0005; label = '0.5 pc'

    return bar, label


# this is my routine to make image
def make_projected_image(p, wt, h=None, method='simple', coords='cart', **kwargs):
    
    # explicit keywords:
    ## h    - smoothing length
    ## method - 'count', 'simple' or 'smooth'
    #
    # other available keywords:
    ## cen  - center of the image
    ## L    - side length along x and y direction
    ## Lz   - depth along z direction, for particle trimming
    ## Nx   - number of pixels along x and y direction, default 250
    ## vmin - minimum scaling of the colormap
    ## vmax - maximum scaling of the colormap
    ## theta, phi, psi - viewing angle
       
    # calculate center if one not given
    if 'cen' in kwargs:
        cen = kwargs['cen']
        xc, yc, zc = cen[0], cen[1], cen[2]
    else:
        xc = np.median(p[:,0])
        yc = np.median(p[:,1])
        zc = np.median(p[:,2])
        
    # coordinate rotation
    import algebra
    theta = kwargs['theta'] if 'theta' in kwargs else 0.
    phi = kwargs['phi'] if 'phi' in kwargs else 0.
    psi = kwargs['psi'] if 'psi' in kwargs else 0.
    x, y, z = algebra.CoordinatesRotate(p[:,0]-xc, p[:,1]-yc, p[:,2]-zc, theta, phi, psi)
        
    # coordinate system
    if coords not in ['cart','azi','quad']: coords = 'cart'
    if (coords == 'azi'):
        r = np.sqrt(y**2+z**2)*np.sign(y); y, z = r, r
    if (coords == 'quad'):
        r = np.sqrt(y**2+z**2); x, y, z = np.abs(x), r, r
        
    # calculate L if one not given
    if 'L' not in kwargs:
        Lx = np.max(np.abs(x))
        Ly = np.max(np.abs(y))
        Lz = np.max(np.abs(z))
        L = max(Lx, Ly, Lz)
    else:
        L = kwargs['L']
    Lz = kwargs['Lz'] if 'Lz' in kwargs else L
    
    # set boundaries and grid size
    Nx = kwargs['Nx'] if 'Nx' in kwargs else 250
    le = [0,0] if coords=='quad' else [-L,-L]
    re, N = [L,L], [Nx,Nx]
    
    # specify the units
    UnitMass_in_Msun = kwargs['UnitMass_in_Msun'] if 'UnitMass_in_Msun' in kwargs else 1e10
    UnitLength_in_pc = kwargs['UnitLength_in_pc'] if 'UnitLength_in_pc' in kwargs else 1e3
    
    # trim the particle, set the boundary for particle deposit
    ok = (x>-L) & (x<L) & (y>-L) & (y<L) & (z>-Lz) & (z<Lz)
    x, y, wt = x[ok], y[ok], wt[ok]
    if (method=='smooth') and (h is not None): h = h[ok]

    # now, call the routine
    from c_library.deposit import deposit
    H = deposit(x, y, h=h, wt=wt, le=le, re=re, N=N, method=method)
    if coords=='cart':
        dx = 2*L/Nx # cell size
        H /= (dx*dx) # surface density
        H *= (UnitMass_in_Msun/UnitLength_in_pc**2) # in Msun per pc^2
    if coords=='azi':
        for i in range(Nx):
            dx = 2*L/Nx
            for j in range(Nx):
                y1, y2 = -L+j*dx, -L+(j+1)*dx
                dV = np.pi*np.abs(y2**2-y1**2)*dx # cell volume
                H[i,j] /= dV # density
        H *= (UnitMass_in_Msun/UnitLength_in_pc**3) # in Msun per pc^3
    if coords=='quad':
        for i in range(Nx):
            dx = L/Nx
            for j in range(Nx):
                y1, y2 = j*dx, (j+1)*dx
                dV = np.pi*np.abs(y2**2-y1**2)*dx # cell volume
                H[i,j] /= dV # density
        H *= (UnitMass_in_Msun/UnitLength_in_pc**3) # in Msun per pc^3
    if (kwargs['draw']==0): return H # just compute image, don't make the plot
    
    # determine color limits
    vmax = kwargs['vmax'] if 'vmax' in kwargs else 1.1*np.max(H)
    vmin = kwargs['vmin'] if 'vmin' in kwargs else max(vmax/1.0e4,min(np.min(H[H>0])/1.1,vmax/100.))
    vmax_max = kwargs['vmax_max'] if 'vmax_max' in kwargs else 1e30
    if (vmax>vmax_max): vmax = vmax_max
    vmax_min = kwargs['vmax_min'] if 'vmax_min' in kwargs else 1e-30
    if (vmax<vmax_min): vmax = vmax_min
    vmin_max = kwargs['vmin_max'] if 'vmin_max' in kwargs else 1e30
    if (vmin>vmin_max): vmin = vmin_max
    vmin_min = kwargs['vmin_min'] if 'vmin_min' in kwargs else 1e30
    if (vmin>vmin_min): vmin = vmin_min
    cmap = kwargs['cmap'] if 'cmap' in kwargs else 'gnuplot2'
    interpolation = kwargs['interpolation'] if 'interpolation' in kwargs else 'none'

    # set up the plot
    if 'show' not in kwargs: kwargs['show'] = 1
    if 'save' not in kwargs: kwargs['save'] = 0
    
    import matplotlib
    if (kwargs['show']==0): matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from matplotlib import rc
    from matplotlib.colors import LogNorm
    
    rc('axes', linewidth=1, labelsize=28)
    rc('xtick.major', size=7, width=1, pad=8)
    rc('xtick', direction='in', labelsize=24)
    rc('text', usetex=True)
    rc('font', size=28, family='DejaVu Sans', serif='Palatino')
                                       
    # set up the plot
    fig = plt.figure(0, (6.88,8))
    fig.subplots_adjust(left=0, right=1, top=1, bottom=0)
    #fig.patch.set_facecolor('k')
    ax = fig.add_axes([0.002,0.14,0.996,0.86])
    ax.patch.set_facecolor('k')
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax_cbar = fig.add_axes([0.25,0.120,0.50,0.013])
    ax_cbar.tick_params(axis='x', colors='k')
                                       
    p = ax.imshow(H.transpose(), origin='lower', extent=(-L,L,-L,L), interpolation=interpolation, norm=LogNorm(vmin=vmin,vmax=vmax), cmap=cmap)
    ax.set_xlim(-L,+L)
    ax.set_ylim(-L,+L)
    cbar = plt.colorbar(p, cax=ax_cbar, orientation='horizontal')
    pindex = 2 if coords=='cart' else 3
    cbar.set_label(r"$\mathrm{M_{\odot}\,pc^{-%d}}$"%pindex, color='k', labelpad=3)
    if 'cticks' in kwargs: cbar.set_ticks(kwargs['cticks'])
    if 'cticklabels' in kwargs: cbar.set_ticklabels(kwargs['cticklabels'])
                                       
    # add scale bar
    bar, label = find_scale_bar(L*UnitLength_in_pc/1e3)
    ax.plot([-0.7*L-bar/2,-0.7*L+bar/2], [-0.82*L,-0.82*L], '-', c='w', lw=3)
    ax.annotate(label, (0.15,0.07), xycoords='axes fraction', color='w', ha='center', va='top', size=28)
                                       
    # add time label
    if 'time' in kwargs:
        ax.annotate(kwargs['time'], (0.98,0.98), xycoords='axes fraction', color='w', ha='right', va='top', size=28)
                                       
    if (kwargs['show']==1): plt.show()
    if (kwargs['save']==1):
        fname = kwargs['fname'] if 'fname' in kwargs else "image.png"
        fig.savefig(fname, format='png', dpi=120)
        plt.clf()
    
    return H
    
    
# plot the phase diagram
def plot_phase_diagram(H, range=[[-2,6],[1,8]], bins=[80,70], **kwargs):

    if 'show' not in kwargs: kwargs['show'] = 1
    if 'save' not in kwargs: kwargs['save'] = 0

    import matplotlib
    if (kwargs['show']==0): matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from matplotlib.colors import LogNorm
    import plotsetting
    
    fig = plt.figure(0, (6.4,4.8))
    fig.subplots_adjust(top=0.975, bottom=0.145)
    ax = fig.add_subplot(111)
    
    ax.imshow(H.transpose(), origin='lower', extent=(range[0][0],range[0][1],range[1][0], range[1][1]), norm=LogNorm(), cmap='jet')
    
    ax.set_xlabel(r"$\log n$ [cm$^{-3}$]")
    ax.set_ylabel(r"$\log T$ [K]")
    
    if (kwargs['show']==1): plt.show()
    if (kwargs['save']==1):
        fname = kwargs['fname'] if 'fname' in kwargs else "phase.pdf"
        fig.savefig(fname, format='pdf')
        plt.clf()

    return
    
    
# get a color map
class Colormap:

    def __init__(self, cmap='coolwarm', vmin=0, vmax=1, scale=None):
    
        import matplotlib
        norm = matplotlib.colors.LogNorm() if scale=='log' else None
        self.p = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
        self.p.set_clim(vmin=vmin, vmax=vmax); self.scale = scale
    
        return
        
    # update colormap
    def update(self, cmap=None, vmin=None, vmax=None, scale=None):
        
        if cmap is None: cmap = self.p.get_cmap()
        if vmin is None: vmin = self.p.get_clim()[0]
        if vmax is None: vmax = self.p.get_clim()[1]
        
        import matplotlib
        norm = matplotlib.colors.LogNorm() if scale=='log' else None
        self.p = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
        self.p.set_clim(vmin=vmin, vmax=vmax); self.scale = scale
    
        return
        
    # get color
    def get_color(self, v):
    
        return self.p.to_rgba(v)


# this calls PFH visualization routine
def image_maker(sp, ptype, **kwargs):
    
    # available keywords
    ## cen  - center of the image
    ## L    - side length along x and y direction
    ## Lz   - depth along z direction, for particle trimming
    ## idir - image of the directory
    ## fbase - file name base of the image
    #
    # many keywords from PFH image_maker routine, see details below
    ## theta, phi - viewing angle
    ## maxden, dynrange - color scaling of the image

    if (sp.k==-1): return -1 # no snapshot
    
    # derived keywords that will be sent to the routine
    center = kwargs['cen'] if 'cen' in kwargs else [0.,0.,0.]
    L = kwargs['L'] if 'L' in kwargs else 10.0
    Lz = kwargs['Lz'] if 'Lz' in kwargs else L
    xrange, yrange, zrange = [-L,L], [-L,L], [-Lz,Lz]
    show_gasstarxray = 'gas' if ptype==0 else 'star'
    idir = kwargs['idir'] if 'idir' in kwargs else sp.sdir+"/images"
    if (not os.path.exists(idir)): os.system("mkdir -p "+idir)
    fbase = kwargs['fbase'] if 'fbase' in kwargs else "snap%03d" %sp.snum
    filename = idir + "/" + fbase + "_" + show_gasstarxray
    cosmo = 1 if sp.cosmological==1 else 0
    
    # these keywords are taken from PFH image_maker routine
    # expand this is we use more features in the future
    theta = kwargs['theta'] if 'theta' in kwargs else 0.0
    phi = kwargs['phi'] if 'phi' in kwargs else 0.0
    dynrange = kwargs['dynrange'] if 'dynrange' in kwargs else 1.0e2
    maxden = kwargs['maxden'] if 'maxden' in kwargs else 0.0
    maxden_rescale = kwargs['maxden_rescale'] if 'maxden_rescale' in kwargs else 1.0
    do_with_colors = kwargs['do_with_colors'] if 'do_with_colors' in kwargs else 1
    threecolor = kwargs['threecolor'] if 'threecolor' in kwargs else 1
    nasa_colors = kwargs['nasa_colors'] if 'nasa_colors' in kwargs else 1
    sdss_colors = kwargs['sdss_colors'] if 'sdss_colors' in kwargs else 0
    project_to_camera = kwargs['project_to_camera'] if 'project_to_camera' in kwargs else 0
    include_lighting = kwargs['include_lighting'] if 'include_lighting' in kwargs else 0
    
    # construct the keywords that will be send to the routine
    keys = {'xrange':xrange, 'yrange':yrange, 'zrange':zrange,
            'center':center, 'cosmo':cosmo, 'theta':theta, 'phi':phi,
            'show_gasstarxray':show_gasstarxray,
            'set_filename':1, 'filename':filename,
            'snapdir_master':"", 'outdir_master':"",
            'dynrange':dynrange, 'maxden':maxden,
            'maxden_rescale':maxden_rescale,
            'do_with_colors':do_with_colors, 'threecolor':threecolor,
            'nasa_colors':nasa_colors, 'sdss_colors':sdss_colors,
            'project_to_camera':project_to_camera,
            'include_lighting':include_lighting}
    
    # call the routine
    from pfh.visualization.image_maker import image_maker as imaker
    image24, massmap = imaker(sp.sdir, sp.snum, **keys)
            
    return massmap


# make threeband image from massmap
def make_threeband_image(fbase, itype='star', L=10.0, tlabel='', maxden=0.01, dynrange=200, save=False):

    # import library, set up plot
    import matplotlib.pyplot as plt
    from matplotlib import rc
    rc('font', size=30, family='serif')

    image = return_threeband_image(fbase, itype=itype, maxden=maxden, dynrange=dynrange)
    
    # set up the plot
    fig = plt.figure(0, (8,8))
    fig.subplots_adjust(left=0, right=1, top=1, bottom=0)
    ax = fig.add_subplot(111)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    
    p = ax.imshow(image, origin='lower', extent=(-L,L,-L,L), interpolation='bicubic')
    ax.set_xlim(-L,+L)
    ax.set_ylim(-L,+L)
                  
    # add scale bar
    bar, label = find_scale_bar(L)
    ax.plot([-0.76*L-bar/2,-0.76*L+bar/2], [-0.81*L,-0.81*L], '-', c='w', lw=3)
    ax.annotate(label, (0.12,0.08), xycoords='axes fraction', color='w', ha='center', va='top')
                  
    # add time label
    ax.annotate(tlabel, (0.12,0.92), xycoords='axes fraction', color='w', ha='center', va='bottom')
    if (save): plt.savefig(fbase+'.jpg', format='jpg')
    plt.show()

    return


# return threeband image from massmap
def return_threeband_image(fbase, itype='star', maxden=0.01, dynrange=200):
    
    import h5py
    import pfh.visualization.make_threeband_image as mti
    
    # read massmap data
    f = h5py.File(fbase+'.hdf5', 'r')
    massmap = f["massmap"][...]
    r, g, b = massmap[:,:,0], massmap[:,:,1], massmap[:,:,2]
    f.close()
    
    image24, cmap = mti.make_threeband_image_process_bandmaps(r, g, b, maxden=maxden, dynrange=dynrange)
    if (itype=='gas'):
        image = mti.layer_band_images(image24, cmap)
    else:
        image = image24
    
    return image
