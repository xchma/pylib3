import numpy as np
import constant

pc = constant.PhysicalConstant()

# gas mean molecular weight and temperature
def gas_mu(ne):
    
    XH = 0.76
    YHe = (1.0-XH) / (4.0*XH)
    
    return (1.0+4.0*YHe) / (1.0+YHe+ne)


def gas_temperature(u, ne, keV=0):
    
    mu = gas_mu(ne)
    gamma = 5.0/3.0
    T = mu*pc.mH*(gamma-1.0)*(u*1.e10)/pc.kB
    
    # in units keV
    if (keV==1): T *= (kB/pc.keV)
    
    return T
    
    
# COM for a list of particles
def center_of_mass(pos, m):

    return m@pos / np.sum(m)


# angular momentum
def angular_momemtum(pos, v, m, cen=None, vcen=None):

    if cen is None: cen = center_of_mass(pos, m)
    if vcen is None: vcen = m@v / np.sum(m)
    j = np.zeros(pos.shape)
    j[:,0] = (pos[:,1]-cen[1])*(v[:,2]-vcen[2]) - (pos[:,2]-cen[2])*(v[:,1]-vcen[1])
    j[:,1] = (pos[:,2]-cen[2])*(v[:,0]-vcen[0]) - (pos[:,0]-cen[0])*(v[:,2]-vcen[2])
    j[:,2] = (pos[:,0]-cen[0])*(v[:,1]-vcen[1]) - (pos[:,1]-cen[1])*(v[:,0]-vcen[0])

    return m@j


# star formation history
def get_SFH(sft, m, dt=0.01, cum=False, m_in_solar=1e10, t_in_yr=1e9):
    
    # get cumulative SFH
    index = np.argsort(sft)
    sft_sorted, m_sorted = sft[index], m[index]
    m_cum = np.cumsum(m_sorted)
    
    # get a time grid
    tmin, tmax = np.min(sft), np.max(sft)
    tbin = max(1+np.int((tmax-tmin)/dt),1000)
    t = np.linspace(tmin, tmax, tbin)
    
    # function to get stellar mass at a given time
    from scipy.interpolate import interp1d
    get_stellar_mass = interp1d(sft_sorted, m_cum, kind='previous', bounds_error=False, fill_value=(0,m_cum[-1]))
    
    # convert to SFR
    if (cum==True):
        sfr = m_in_solar*get_stellar_mass(t)
    else:
        mass_now = m_in_solar*get_stellar_mass(t)
        mass_former = m_in_solar*get_stellar_mass(t-dt)
        sfr = (mass_now-mass_former)/(t_in_yr*dt)

    return t, sfr
