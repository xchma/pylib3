#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "allvar.h"


// read paramter file
void read_param_file(char* fname)
{
#define INT 1
#define STRING 2
#define DOUBLE 3
#define MAXTAGS 300
    
    if (ThisTask==0) printf("Read parameter file\n");
    
    // check if file exists
    FILE* fp;
    if (!(fp=fopen(fname,"r")))
    {
        if (ThisTask==0)
            printf("Fatal error: cannot open parameter file %s.\n", fname);
        endrun(2);
    }
    
    // adapted from GIZMO
    char buf[300], buf1[50], buf2[200], buf3[50];
    int i, j, nt, errorFlag=0;
    int id[MAXTAGS];
    void *addr[MAXTAGS];
    char tag[MAXTAGS][50];
    
    nt = 0;
    strcpy(tag[nt], "BpassDir");
    addr[nt] = &All.BpassDir;
    id[nt++] = STRING;
    
    strcpy(tag[nt], "BpassIMF");
    addr[nt] = &All.BpassIMF;
    id[nt++] = STRING;
    
    strcpy(tag[nt], "BinaryFlag");
    addr[nt] = &All.BinaryFlag;
    id[nt++] = INT;
    
    strcpy(tag[nt], "InputFile");
    addr[nt] = &All.InputFile;
    id[nt++] = STRING;
    
    strcpy(tag[nt], "OutputDir");
    addr[nt] = &All.OutputDir;
    id[nt++] = STRING;
    
    strcpy(tag[nt], "OutputFile");
    addr[nt] = &All.OutputFile;
    id[nt++] = STRING;
    
    strcpy(tag[nt], "DustTable");
    addr[nt] = &All.DustTable;
    id[nt++] = STRING;
    
    strcpy(tag[nt], "DustType");
    addr[nt] = &All.DustType;
    id[nt++] = STRING;
    
    strcpy(tag[nt], "DTM");
    addr[nt] = &All.DTM;
    id[nt++] = DOUBLE;
    
    // add more parameters here
    
    while (!feof(fp))
    {
        fgets(buf, 300, fp);
        if(sscanf(buf,"%s%s%s",buf1,buf2,buf3)<2)
            continue;
        if(buf1[0]=='#')
            continue;
        
        for(i=0,j=-1;i<nt;i++)
        {
            if(strcmp(buf1,tag[i])==0)
            {
                j = i;
                tag[i][0] = 0;
                break;
            }
        }
        
        if (j>=0)
        {
            switch (id[j]) {
                case INT:
                    *((int *)addr[j]) = atoi(buf2);
                    break;
                case STRING:
                    strcpy((char *)addr[j], buf2);
                    break;
                case DOUBLE:
                    *((double *)addr[j]) = atof(buf2);
            }
        }
    }
    fclose(fp);
    
    for (i=0; i<nt; i++)
    {
        if (*tag[i]) {
            if (ThisTask==0) printf("Error: tag '%s' is missing\n", tag[i]);
            errorFlag = 1;
        }
    }
    
    if (errorFlag==1)
    {
        if (ThisTask==0) printf("Error: missing parameter(s).\n");
        endrun(2);
    }

#undef INT
#undef STRING
#undef DOUBLE
#undef MAXTAGS
}


void read_input()
{
    if (ThisTask==0) printf("Read input file\n");
    
    hid_t file_id = H5Fopen(All.InputFile, H5F_ACC_RDONLY, H5P_DEFAULT);
    herr_t status;
    
    status = H5LTget_attribute_int(file_id, "/", "Nstar", &Ns);
    status = H5LTget_attribute_int(file_id, "/", "Nwave", &Np);
    
    // allocate memory
    agearray = malloc(Ns*sizeof(double));
    zarray = malloc(Ns*sizeof(double));
    marray = malloc(Ns*sizeof(double));
    varray = malloc(Ns*sizeof(double));
    Sarray = malloc(Ns*sizeof(double));
    Uarray = malloc(Ns*sizeof(double));
    wave = malloc(Np*sizeof(double));
    kappa = malloc(Np*sizeof(double));
    
    // read sources
    status = H5LTread_dataset_double(file_id, "/age", agearray);
    status = H5LTread_dataset_double(file_id, "/z", zarray);
    status = H5LTread_dataset_double(file_id, "/m", marray);
    status = H5LTread_dataset_double(file_id, "/v", varray);
    status = H5LTread_dataset_double(file_id, "/U", Uarray);
    status = H5LTread_dataset_double(file_id, "/column", Sarray);
    status = H5LTread_dataset_double(file_id, "/wave", wave);
    
    H5Fclose(file_id);
    
    // read dust table
    wt = malloc(Nt*sizeof(double));
    opacity = malloc(Nt*sizeof(double));
    
    // do the reading
    file_id = H5Fopen(All.DustTable, H5F_ACC_RDONLY, H5P_DEFAULT);
    status = H5LTread_dataset_double(file_id, "/wave", wt);
    status = H5LTread_dataset_double(file_id, All.DustType, opacity);
    H5Fclose(file_id);
    
    // calculate opacity
    int i,j;
    for (i=0,j=0; j<Np; j++)
    {
        while (wt[i+1]<=wave[j]) i++;
        kappa[j] = opacity[i] + (opacity[i+1]-opacity[i])*(wave[j]-wt[i])/(wt[i+1]-wt[i]);
    }
}
