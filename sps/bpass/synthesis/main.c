#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>

#include "allvar.h"


int main(int argc, char** argv)
{
    // initialize MPI
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &ThisTask);
    MPI_Comm_size(MPI_COMM_WORLD, &NTask);
    
    if (argc<2)
    {
        if (ThisTask==0) printf("Usage: synthesis <ParamFile>\n");
        endrun(0);
    }
    
    // read parameter file
    char ParamFile[200];
    strcpy(ParamFile, argv[1]);
    read_param_file(ParamFile);
    mkdir(All.OutputDir, 02755);
    
    // read input
    read_input();
    
    // stellar continuum
    do_stellar();
    
    // nebular emission
    do_nebular();
    
    // finalize
    free_memory();
    MPI_Finalize();
    
    return 0;
}


void free_memory()
{
    free(agearray);
    free(marray);
    free(zarray);
    free(varray);
    free(Sarray);
    free(Uarray);
    free(wave);
    free(kappa);
    free(wt);
    free(opacity);
}


void endrun(int ierr)
{
    if (ThisTask==0) printf("Endrun called at point %d\n", ierr);
    MPI_Finalize();
    exit(0);
}
