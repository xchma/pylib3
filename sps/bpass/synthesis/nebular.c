#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "allvar.h"

#define NU 7
#define NZ 7
#define NA 15
#define NW 100000

double *ages;
double *wavs;
double *opas;
double ***libline;
double ***libcont;

static double LOGU[NU] = {-4,-2.5,-3,-2.5,-2,-1.5,-1};
static double ZMET[NZ] = {0.005,0.02,0.2,0.4,1.0,2.5,5.0};


const char* get_nebular_file(int mode)
{
    sprintf(TableFile, "%s_%s/nebular/nebular-", All.BpassDir, All.BpassIMF);
    if (mode==0) strcat(TableFile, "line-");
    else strcat(TableFile, "cont-");
    
    if (All.BinaryFlag==0) strcat(TableFile, "bin-");
    else strcat(TableFile, "sin-");
    
    strcat(TableFile, All.BpassIMF);
    strcat(TableFile, ".h5");
    
    return TableFile;
}


void read_nebular()
{
    if (ThisTask==0) printf("Read nebular emission\n");
    
    // allocate memory
    ages = malloc(NA*sizeof(double));
    wavs = malloc(NL*sizeof(double));
    opas = malloc(NL*sizeof(double));
    libline = malloc(NU*sizeof(double**));
    libcont = malloc(NU*sizeof(double**));
    
    int i,j;
    for (i=0; i<NU; i++)
    {
        libline[i] = malloc(NZ*sizeof(double*));
        libcont[i] = malloc(NZ*sizeof(double*));
        for (j=0; j<NZ; j++)
        {
            libline[i][j] = malloc(NA*NL*sizeof(double));
            libcont[i][j] = malloc(NA*NW*sizeof(double));
        }
    }
    
    // read emission line table
    char fname[200], path[20];
    strcpy(fname, get_nebular_file(0));
    
    hid_t file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    herr_t status = H5LTread_dataset_double(file_id, "/ages", ages);
    status = H5LTread_dataset_double(file_id, "/wavs", wavs);
    
    for (i=0; i<NU; i++)
        for (j=0; j<NZ; j++)
        {
            sprintf(path, "/LOGU_%.1f/z%.3f", LOGU[i], ZMET[j]);
            status = H5LTread_dataset_double(file_id, path, libline[i][j]);
        }
    
    H5Fclose(file_id);
    
    // read nebular continuum table
    strcpy(fname, get_nebular_file(1));
    
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    
    for (i=0; i<NU; i++)
        for (j=0; j<NZ; j++)
        {
            sprintf(path, "/LOGU_%.1f/z%.3f", LOGU[i], ZMET[j]);
            status = H5LTread_dataset_double(file_id, path, libcont[i][j]);
        }
    
    H5Fclose(file_id);
    
    // get opacity for lines
    for (i=0; i<NL; i++)
    {
        j=0; do j++; while (wt[j+1]<=wavs[i]);
        opas[i] = opacity[j] + (opacity[j+1]-opacity[j])*(wavs[i]-wt[j])/(wt[j+1]-wt[j]);
    }
}


void write_nebular()
{
    printf("Write nebular emission\n");
    
    char fname[200];
    sprintf(fname, "%s/%s", All.OutputDir, All.OutputFile);
    
    // add to existing file
    hid_t file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
    herr_t status;
    
    hsize_t dims[1] = {NL};
    status = H5LTmake_dataset_double(file_id, "linewavs", 1, dims, wavs);
    status = H5LTmake_dataset_double(file_id, "linelums", 1, dims, lines);
    
    dims[0] = Np;
    status = H5LTmake_dataset_double(file_id, "nebcont", 1, dims, ncont);
    
    H5Fclose(file_id);
}


void free_nebular()
{
    int i,j;
    for (i=0; i<NU; i++)
    {
        for (j=0; j<NZ; j++)
        {
            free(libline[i][j]);
            free(libcont[i][j]);
        }
        free(libline[i]);
        free(libcont[i]);
    }
    free(ages);
    free(wavs);
    free(opas);
    free(libline);
    free(libcont);
    free(lines);
    free(ncont);
}


void do_nebular()
{
    // allocate memory for output
    lines = malloc(NL*sizeof(double));
    ncont = malloc(Np*sizeof(double));
    
    // read table
    read_nebular();
    
    int i,j,k,iU,iz,ia,iw;
    double m,z,U,S,age,logz,loga;
    double ul,ur,zl,zr,al,ar,wl,wr;
    double Cul,Cur,Czl,Czr,Cal,Car,Cwl,Cwr;
    double A1,A2,A3,A4,A5,A6,A7,A8;
    double Qlll,Qllr,Qlrl,Qlrr;
    double Qrll,Qrlr,Qrrl,Qrrr,Q,Q2[2];
    
    // loop over stars
    for (i=ThisTask; i<Ns; i+=NTask)
    {
        m = marray[i]; // relative
        U = Uarray[i];
        z = zarray[i]/0.14; // in solar
        age = 1e9*agearray[i]; // Gyr to yr
        S = All.DTM*Sarray[i]; // dust column in g cm^-2
        
        if (U<-4) U=-4;
        if (U>-1.001) U=-1.001;
        if (z<0.005) z=0.005;
        if (z>4.999) z=4.999;
        if (age<1e6) age=1e6;
        if (age>2.511e7) continue;
        
        iU=0; while (LOGU[iU+1]<=U) iU++;
        iz=0; while (ZMET[iz+1]<=z) iz++;
        ia=0; while (ages[ia+1]<=age) ia++;
        
        ul = LOGU[iU];
        ur = LOGU[iU+1];
        zl = log10(ZMET[iz]);
        zr = log10(ZMET[iz+1]);
        al = log10(ages[ia]);
        ar = log10(ages[ia+1]);
        logz = log10(z);
        loga = log10(age);
        
        Cul = (U-ul)/(ur-ul);
        Cur = (ur-U)/(ur-ul);
        Czl = (logz-zl)/(zr-zl);
        Czr = (zr-logz)/(zr-zl);
        Cal = (loga-al)/(ar-al);
        Car = (ar-loga)/(ar-al);
        
        A1 = Cur*Czr*Car;
        A2 = Cur*Czr*Cal;
        A3 = Cur*Czl*Car;
        A4 = Cur*Czl*Cal;
        A5 = Cul*Czr*Car;
        A6 = Cul*Czr*Cal;
        A7 = Cul*Czl*Car;
        A8 = Cul*Czl*Cal;
        
        // emission lines
        for (j=0; j<NL; j++)
        {
            Qlll = *(libline[iU][iz]+ia*NL+j);
            Qllr = *(libline[iU][iz]+(ia+1)*NL+j);
            Qlrl = *(libline[iU][iz+1]+ia*NL+j);
            Qlrr = *(libline[iU][iz+1]+(ia+1)*NL+j);
            Qrll = *(libline[iU+1][iz]+ia*NL+j);
            Qrlr = *(libline[iU+1][iz]+(ia+1)*NL+j);
            Qrrl = *(libline[iU+1][iz+1]+ia*NL+j);
            Qrrr = *(libline[iU+1][iz+1]+(ia+1)*NL+j);
            Q = A1*Qlll + A2*Qllr + A3*Qlrl + A4*Qlrr + A5*Qrll + A6*Qrlr + A7*Qrrl + A8*Qrrr;
            lines[j] += Q*m*exp(-opas[j]*S);
        }
        
        // nebular continuum
        for (j=0; j<Np; j++)
        {
            iw = (int)wave[j];
            wl = (double)iw;
            wr = wl + 1.0;
            Cwl = (wave[j]-wl)/(wr-wl);
            Cwr = (wr-wave[j])/(wr-wl);
            
            for (k=0; k<2; k++)
            {
                iw = (int)wave[j];
                Qlll = *(libcont[iU][iz]+ia*NW+iw+k);
                Qllr = *(libcont[iU][iz]+(ia+1)*NW+iw+k);
                Qlrl = *(libcont[iU][iz+1]+ia*NW+iw+k);
                Qlrr = *(libcont[iU][iz+1]+(ia+1)*NW+iw+k);
                Qrll = *(libcont[iU+1][iz]+ia*NW+j);
                Qrlr = *(libcont[iU+1][iz]+(ia+1)*NW+iw+k);
                Qrrl = *(libcont[iU+1][iz+1]+ia*NW+iw+k);
                Qrrr = *(libcont[iU+1][iz+1]+(ia+1)*NW+iw+k);
                Q2[k] = A1*Qlll + A2*Qllr + A3*Qlrl + A4*Qlrr + A5*Qrll + A6*Qrlr + A7*Qrrl + A8*Qrrr;
            }
            Q = Cwr*Q2[0] + Cwl*Q2[1];
            ncont[j] += Q*m*exp(-kappa[j]*S);
        }
    }
    
    // communicate
    double buf;
    for (i=0; i<NL; i++)
    {
        MPI_Allreduce(&lines[i], &buf, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        lines[i] = buf; // lines
    }
    
    for (i=0; i<Np; i++)
    {
        MPI_Allreduce(&ncont[i], &buf, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        ncont[i] = buf; // nebular continuum
    }
    
    // write lines
    if (ThisTask==0) write_nebular();
    
    // free memory
    free_nebular();
}
