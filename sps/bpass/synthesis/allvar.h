#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// MPI
int ThisTask, NTask;

// constant
#define CC 2.99792458e5

// parameters
struct Parameters
{
    char BpassDir[200];
    char BpassIMF[100];
    int BinaryFlag;
    char InputFile[200];
    char OutputDir[200];
    char OutputFile[200];
    char DustTable[200];
    char DustType[10];
    double DTM;
} All;

// file name
char TableFile[200];

// extinction curve
#define Nt 600
double *wt;
double *opacity;

// sources
int Ns;
double *agearray;
double *zarray;
double *marray;
double *varray;
double *Sarray;
double *Uarray;

// output spectra
int Np;
double *wave;
double *spectra;
double *kappa;

// nebular
#define NL 125
double *lines; // emission lines
double *ncont; // nebular continuum

// function
void read_param_file(char* fname);
void read_input(void);
void free_memory(void);
void endrun(int ierr);
void do_stellar(void);
void do_nebular(void);
