#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <hdf5.h>
#include <hdf5_hl.h>

#include "allvar.h"

#define Nz 13
#define Nw 100000
#define Na 51
double **table;
double *starmass;

static double Z[Nz]={1e-5,1e-4,0.001,0.002,0.003,0.004,0.006,0.008,0.010,0.014,0.020,0.030,0.040};


const char* get_metallicity_file(int i)
{
    sprintf(TableFile, "%s_%s/spectra/spectra-", All.BpassDir, All.BpassIMF);
    
    if (All.BinaryFlag==0)
        strcat(TableFile, "bin-");
    else
        strcat(TableFile, "sin-");
    
    strcat(TableFile, All.BpassIMF);
    
    char tmp[10];
    if (i==0)
        strcpy(tmp, ".zem5");
    else if (i==1)
        strcpy(tmp, ".zem4");
    else
        sprintf(tmp, ".z%03d", (int)(1e3*Z[i]));
    
    strcat(TableFile, tmp);
    strcat(TableFile, ".h5");
    
    return TableFile;
}


const char* get_starmass_file()
{
    sprintf(TableFile, "%s_%s/starmass/starmass-", All.BpassDir, All.BpassIMF);
    
    if (All.BinaryFlag==0)
        strcat(TableFile, "bin-");
    else
        strcat(TableFile, "sin-");
    
    strcat(TableFile, All.BpassIMF);
    strcat(TableFile, ".h5");
    
    return TableFile;
}


// read table
void read_stellar()
{
    if (ThisTask==0) printf("Read BPASS continuum\n");
    
    // declare
    hid_t file_id;
    herr_t status;
    
    // read spectra
    table = malloc(Nz*sizeof(double*));
    
    int i;
    char fname[200];
    
    for (i=0; i<Nz; i++)
    {
        // allocate memory
        table[i] = malloc(Nw*Na*sizeof(double));
        
        // file name
        strcpy(fname, get_metallicity_file(i));
        
        // do the reading
        file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
        status = H5LTread_dataset_double(file_id, "/spectra", table[i]);
        H5Fclose(file_id);
    }
    
    // read star mass
    starmass = malloc(Na*Nz*sizeof(double));
    
    // file name
    strcpy(fname, get_starmass_file());
    
    // do the reading
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
    status = H5LTread_dataset_double(file_id, "/total", starmass);
    H5Fclose(file_id);
}


void write_stellar()
{
    printf("Write stellar continuum\n");
    
    char fname[200];
    sprintf(fname, "%s/%s", All.OutputDir, All.OutputFile);
    
    // create a new file
    hid_t file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    herr_t status;
    
    hsize_t dims[1] = {Np};
    status = H5LTmake_dataset_double(file_id, "wavelength", 1, dims, wave);
    status = H5LTmake_dataset_double(file_id, "spectra", 1, dims, spectra);
    
    H5Fclose(file_id);
}


void free_stellar()
{
    int i;
    for (i=0; i<Nz; i++) free(table[i]);
    free(table);
    free(spectra);
    free(starmass);
}


void do_stellar()
{
    // allocate memory for output
    spectra = malloc(Np*sizeof(double));
    
    // read table
    read_stellar();
    
    int i,j,k,ia,iz,iw;
    double m,z,v,w,age,S,logz,loga;
    double al,ar,wl,wr,zl,zr;
    double Qll,Qlr,Qrl,Qrr,Q2[2],Q;
    double Mll,Mlr,Mrl,Mrr,M;
    double Cal,Car,Cwl,Cwr,Czl,Czr;
    
    // loop over stars
    for (i=ThisTask; i<Ns; i+=NTask)
    {
        m = marray[i];
        z = zarray[i];
        v = varray[i];
        age = 1e9*agearray[i]; // Gyr to yr
        S = All.DTM*Sarray[i]; // dust column in g cm^-2
        if (z<1e-5) z=1e-5;
        if (z>0.0399) z=0.0399;
        if (age<1e6) age=1e6;
        if (age>2e10) age=2e10;
        logz = log10(z);
        loga = log10(age);
        ia = (int)((loga-6)/0.1);
        iz = 0; while (Z[iz+1]<=z) iz++;
        
        // remaining mass
        al = 6 + 0.1*ia;
        ar = al + 0.1;
        zl = log10(Z[iz]);
        zr = log10(Z[iz+1]);
        Cal = (loga-al)/(ar-al);
        Car = (ar-loga)/(ar-al);
        Czl = (logz-zl)/(zr-zl);
        Czr = (zr-logz)/(zr-zl);
        Mll = *(starmass+iz*Na+ia);
        Mlr = *(starmass+iz*Na+ia+1);
        Mrl = *(starmass+(iz+1)*Na+ia);
        Mrr = *(starmass+(iz+1)*Na+ia+1);
        M = Czr*Car*Mll + Czr*Cal*Mlr + Czl*Car*Mrl + Czl*Cal*Mrr;
        marray[i] = m*(1e6/M); // reserve for now
    
        // loop over wavelengths
        for (j=0; j<Np; j++)
        {
            w = wave[j]*(1-v/CC); // doppler shifted wavelength
            if (w<1) w=1;
            if (w+1e-4>Nw) w=Nw-1e-4;
            iw = (int)w;
            
            for (k=iz; k<iz+2; k++)
            {
                wl = (double)iw;
                wr = wl + 1.0;
                Cwl = (w-wl)/(wr-wl);
                Cwr = (wr-w)/(wr-wl);
                Qll = *(table[k]+Nw*ia+iw);
                Qlr = *(table[k]+Nw*ia+iw+1);
                Qrl = *(table[k]+Nw*(ia+1)+iw);
                Qrr = *(table[k]+Nw*(ia+1)+iw+1);
                Q2[k-iz] = Car*Cwr*Qll + Car*Cwl*Qlr + Cal*Cwr*Qrl + Cal*Cwl*Qrr;
            }
            
            Q = Czr*Q2[0] + Czl*Q2[1];
            spectra[j] += Q*(m/M)*exp(-kappa[j]*S);
        }
    }
    
    // communicate
    double buf;
    for (i=0; i<Np; i++)
    {
        MPI_Allreduce(&spectra[i], &buf, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        spectra[i] = buf;
    }
    
    // write stellar continuum
    if (ThisTask==0) write_stellar();
    
    // free memory
    free_stellar();
}
