# this is our interface with BPASS tables
import numpy as np
import ctypes
import h5py
import os

metallicity_grid = ['em5','em4','001','002','003','004','006','008','010','014','020','030', '040']

# existing bands should not change their IDs
# new bands should be added after these existing ones
BAND_name = ['Q_ion', 'bolometric', '1500', 'GALEX_GALEX.FUV', 'GALEX_GALEX.NUV', 'SLOAN_SDSS.u', 'SLOAN_SDSS.g', 'SLOAN_SDSS.r', 'SLOAN_SDSS.i', 'SLOAN_SDSS.z', '2MASS_2MASS.J', '2MASS_2MASS.H', '2MASS_2MASS.K', 'Johnson.U', 'Johnson.B', 'Johnson.V', 'Johnson.R', 'Johnson.I', 'Q_HI', 'Q_HeI', 'Q_HeII', 'Lya', 'Ha', 'Hb'] # id ranges 0--21
lambda_eff = [912.0, 5000.0, 1500.0, 1542.3, 2274.4, 3594.9, 4640.4, 6122.3, 7439.5, 8897.1, 12350.0, 16620.0, 21590.0, 3656.0, 4353.0, 5477.0, 6349.0, 8797.0, 912.0, 508.3, 228.0, 1215.67, 6562.81, 4861.33] # in Angstrom
nu_eff = [3.2872e15, 5.9958e14, 1.9986e15, 1.9438e15, 1.3181e15, 8.3394e14, 6.4605e14, 4.9867e14, 4.0297e14, 3.3696e14, 2.4275e14, 1.8038e14, 1.3886e14, 8.2000e14, 6.8870e14, 5.4737e14, 4.7219e14, 3.4079e14, 3.2872e15, 5.8976e15, 1.3149e16, 2.4661e15, 4.5681e14, 6.1669e14] # in Hz

# this returns the column ID for a given band
def get_band_ID(band='bolometric'):
    if band in ['ionizing','quanta','Qion','ion']:
        id = 0
    elif band in ['bolometric','bol']:
        id = 1
    elif band in ['1500','UV']:
        id = 2
    elif band in ['GALEX FUV','FUV']:
        id = 3
    elif band in ['GALEX NUV','NUV']:
        id = 4
    elif band in ['Sloan u','SDSS u','u']:
        id = 5
    elif band in ['Sloan g','SDSS g','g']:
        id = 6
    elif band in ['Sloan r','SDSS r','r']:
        id = 7
    elif band in ['Sloan i','SDSS i','i']:
        id = 8
    elif band in ['Sloan z','SDSS z','z']:
        id = 9
    elif band in ['2MASS J','J']:
        id = 10
    elif band in ['2MASS H','K']:
        id = 11
    elif band in ['2MASS K','K']:
        id = 12
    elif band in ['Johnson U','U']:
        id = 13
    elif band in ['Johnson B','B']:
        id = 14
    elif band in ['Johnson V','V']:
        id = 15
    elif band in ['Johnson R','R']:
        id = 16
    elif band in ['Johnson I','I']:
        id = 17
    elif band in ['Q_HI']:
        id = 18
    elif band in ['Q_HeI']:
        id = 19
    elif band in ['Q_HeII']:
        id = 20
    elif band in ['Lya','1216','LyA','Lyman-alpha']:
        id = 21
    elif band in ['Ha','6563','Halpha']:
        id = 22
    elif band in ['Hb','4861','Hbeta']:
        id = 23
    else:
        id = 1
    return id

# get effective wavelength of a given band
def get_band_lambda_eff(band='bolometric'):
    band_id = get_band_ID(band=band)
    return lambda_eff[band_id]

# get effective frequency of a given band
def get_band_nu_eff(band='bolometric'):
    band_id = get_band_ID(band=band)
    return nu_eff[band_id]
    
    
# calculate remaining mass fraction
def compute_mass_loss(age_in_Gyr, z_in_frac, version="BPASSv2.2.1", IMF="imf135_100", binary=False):

    # load mass table
    fn = os.environ['PYLIB'] + '/sps/bpass/%s_%s/starmass'%(version,IMF)
    fn += '-bin' if binary else '-sin'
    fn += '-%s.h5'%IMF
    f = h5py.File(fn, 'r')
    z = f['z'][...]
    age = f['age'][...]/1e9 # grid in Gyr
    table = f['remain'][...] # size by (z,age)
    f.close()
    
    # interpolation (log scale)
    import c_library.interp as itp
    logm = itp.interp2d(np.log10(z), np.log10(age), np.log10(table), np.log10(z_in_frac), np.log10(age_in_Gyr))

    return 10**logm/1e6


# calculate luminosity for a given band
def compute_stellar_luminosity(age_in_Gyr, z_in_frac, version="BPASSv2.2.1", IMF="imf135_100", band='bolometric', binary=False, correct_for_mass_loss=False, return_linear_scale=False):
    
    # load photometry table
    fn = os.environ['PYLIB'] + '/sps/bpass/%s_%s/photometry'%(version,IMF)
    fn += '-bin' if binary else '-sin'
    fn += '-%s.h5'%IMF
    f = h5py.File(fn, 'r')
    z = f['z'][...]
    age = f['age'][...]/1e9 # grid in Gyr
    band_id = get_band_ID(band=band)
    table = f[BAND_name[band_id]][...] # size by (z,age)
    f.close()
    
    # interpolation
    import c_library.interp as itp
    logl = itp.interp2d(np.log10(z), np.log10(age), table, np.log10(z_in_frac), np.log10(age_in_Gyr)) - 6
    
    # correct for mass loss
    if correct_for_mass_loss:
        fremain = compute_mass_loss(age_in_Gyr, z_in_frac, version=version, IMF=IMF, binary=binary)
        logl -= np.log10(fremain) # boost for initial mass
    
    # units:
    # Q_ion, Q_HI, Q_HeI, Q_HeII -- log s^-1 Msun^-1
    # bolometric luminosity -- log ergs s^-1 Msun^-1
    # other bands -- log ergs s^-1 AA^-1 Msun^-1
    
    return 10**logl if return_linear_scale else logl


# some emission lines available
def get_line_ID(line='Lya', id=()):
    if line in ['Lya','Lyman alpha','Lyman-alpha','1216']:
        id += (0,)
    if line in ['Lyb','Lyman beta','Lyman-beta','1026']:
        id += (1,)
    if line in ['Ha','Halpha','H alpha','H-alpha','Balmer alpha','Balmer-alpha','6563']:
        id += (8,)
    if line in ['Hb','Hbeta','H beta','H-beta','Balmer beta','Balmer-beta','4861']:
        id += (9,)
    if line in ['Hgamma','H gamma','H-gamma','Balmer gamma','Balmer-gamma','4340']:
        id += (10,)
    if line.startswith(('He II','HeII','1640',)):
        id += (44,)
    if line.startswith(('C III','CIII','1907','1909',)):
        id += (53,54,)
    if line.startswith(('C IV','CIV','1548',)):
        id += (55,)
    if line.startswith(('N II','NII','6583',)):
        id += (57,)
    if line.startswith(('O III','OIII','[O III]','[OIII]','4959','5007',)):
        id += (77,76,)
    if line in ['O III 88m','[O III] 88m','OIII 88m','[OIII] 88m','88m']:
        id += (80,)
    if line.startswith(('Mg II','MgII','2796','2803',)):
        id += (91,90,)
    return id
    
    
# get luminosity for line ID
def get_line_by_ID(age_in_Gyr, z_in_solar, U_in, line_ID=0, version="BPASSv2.2.1", IMF="imf135_100", binary=False):

    # allocate array
    U = np.array([-4.0,-3.5,-3.0,-2.5,-2.0,-1.5,-1.0], dtype='float64')
    Z = np.array([0.005,0.020,0.200,0.400,1.000,2.500,5.000], dtype='float64')
    table = np.zeros((U.size,Z.size,15))
    
    # load table
    fn = os.environ['PYLIB'] + '/sps/bpass/%s_%s/nebular-line'%(version,IMF)
    fn += '-bin' if binary else '-sin'
    fn += '-%s.h5'%IMF
    f = h5py.File(fn, 'r')
    age = f['ages'][...]/1e9 # grid in Gyr
    for i,u in enumerate(U):
        for j,z in enumerate(Z):
            table[i,j,:] = f['LOGU_%.1f/z%.3f'%(u,z)][:,line_ID]
    f.close()
    
    # interpolation (log scale)
    import c_library.interp as itp
    logl = itp.interp3d(U, np.log10(Z), np.log10(age), np.log10(table), U_in, np.log10(z_in_solar), np.log10(age_in_Gyr))

    return 10**logl


# calculate luminosity for a given line
def compute_line_luminosity(age_in_Gyr, z_in_frac, U_in=-1.5, version="BPASSv2.2.1", IMF="imf135_100", line='Lya', binary=False, correct_for_mass_loss=False):

    # convert to numpy arrays
    age_in_Gyr = np.array(age_in_Gyr, ndmin=1)
    z_in_solar = np.array(z_in_frac, ndmin=1) / 0.014 # this is the built-in value
    if np.isscalar(U_in): U_in = U_in*np.ones(age_in_Gyr.size)
    
    # allocate output
    l = np.zeros(age_in_Gyr.size)
    
    # select eligible stars
    j, = np.where(age_in_Gyr<0.0251188)
    if j.size==0: return l
    
    # do the interpolation
    for line_ID in get_line_ID(line=line):
        l[j] += get_line_by_ID(age_in_Gyr[j], z_in_solar[j], U_in[j], line_ID=line_ID, version=version, IMF=IMF, binary=binary)
    
    # correct for mass loss
    if correct_for_mass_loss:
        fremain = compute_mass_loss(age_in_Gyr[j], z_in_solar[j]*z_solar, version=version, IMF=IMF, binary=binary)
        l /= fremain # boost for initial mass
        
    # units:
    # solar luminsity for a 1-Msun stellar population (initially?)
    
    return l[0] if l.size==0 else l
