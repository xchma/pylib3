import sys
import os

SnapDir = sys.argv[1]
HaloDir = sys.argv[2]
SnapNum = int(sys.argv[3])
# multi-snapshot as 1, single-snapshot as 0
MultiSnapFlag = int(sys.argv[4])

f = open(os.environ['PYLIB']+"/gizmo_lib/AHF/AMIGA.input", "r")
l1 = f.readline()
l2 = f.readline()
l3 = f.readline()
l4 = f.readline()
l5 = f.readline()
l6 = f.readline()
l7 = f.readline()
l8 = f.readline()
l9 = f.readline()
l10 = f.readline()
lrest = f.read()
f.close()

if (MultiSnapFlag):
  l4 = "ic_filename\t= " + SnapDir + "/snap_converteddir_%03d/snap_convertedshot_%03d.\n" % (SnapNum, SnapNum)
else:
  l4 = "ic_filename\t= " + SnapDir + "/snap_convertedshot_%03d\n" % SnapNum

if (MultiSnapFlag):
  l7 = "ic_filetype\t= 61\n"
else:
  l7 = "ic_filetype\t= 60\n"

l10 = "outfile_prefix\t= " + HaloDir + "/snap%03dRPep\n" % SnapNum

f = open(HaloDir+"/AMIGA.input%03d" % SnapNum, "w")
f.write(l1)
f.write(l2)
f.write(l3)
f.write(l4)
f.write(l5)
f.write(l6)
f.write(l7)
f.write(l8)
f.write(l9)
f.write(l10)
f.write(lrest)
f.close()
