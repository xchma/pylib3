import numpy as np
import physics as phys
from c_library.starhsml import get_particle_hsml

# This is the class that describes a galaxy/halo in a given
# snapshot, for either cosmological or isolated simulations.

class Halo:

    def __init__(self, sp):

        # a halo must relate to a snapshot
        self.sp = sp
        self.k = -1 if sp.k==-1 else 0
        self.cosmological = sp.cosmological
        
        self.p0 = sp.p0
        self.p1 = sp.p1
        self.p2 = sp.p2
        self.p3 = sp.p3
        self.p4 = sp.p4
        self.p5 = sp.p5
        self.part = sp.part
        self.time = sp.time
        self.redshift = sp.redshift
        
        return


    # load halo properties
    def load(self, hdir=None, hlid=0, periodic=0):

        if (self.k!=0): return

        sp = self.sp

        # non-cosmological
        if (sp.cosmological==0):

            self.k = 1
            if periodic==0:
                self.xc = 0.0
                self.yc = 0.0
                self.zc = 0.0
                self.rvir = sp.boxsize/2
            else:
                self.xc = sp.boxsize/2
                self.yc = sp.boxsize/2
                self.zc = sp.boxsize/2
                self.rvir = sp.boxsize/2
        
            return

        # cosmological, use AHF
        AHF = sp.loadAHF(hdir=hdir)
        
        if (AHF.k==1):
        
            self.k = 1
            self.id = AHF.ID[hlid]
            self.host = AHF.hostHalo[self.id]
            self.npart = AHF.npart[self.id]
            self.ngas = AHF.n_gas[self.id]
            self.nstar = AHF.n_star[self.id]
            self.mvir = AHF.Mvir[self.id]
            self.mgas = AHF.M_gas[self.id]
            self.mstar = AHF.M_star[self.id]
            self.xc = AHF.Xc[self.id]
            self.yc = AHF.Yc[self.id]
            self.zc = AHF.Zc[self.id]
            self.rvir = AHF.Rvir[self.id]
            self.rmax = AHF.Rmax[self.id]
            self.vxc = AHF.VXc[self.id]
            self.vyc = AHF.VYc[self.id]
            self.vzc = AHF.VZc[self.id]
            self.vmax = AHF.Vmax[self.id]
            self.fhi = AHF.fMhires[self.id]
                
        else:
        
            self.k = 0
            self.xc = sp.boxsize/2
            self.yc = sp.boxsize/2
            self.zc = sp.boxsize/2
            self.rvir = sp.boxsize*2
    
        return


    # load particle
    def loadpart(self, ptype, block_group=()):

        part = self.part[ptype]
        part.load(block_group=block_group)

        return part
    
    
    # get distance from halo center
    def get_distance_from_halo(self, pos, cen=None):
    
        if cen is None: cen = [self.xc,self.yc,self.zc]
        r = np.sqrt((pos[:,0]-cen[0])**2+(pos[:,1]-cen[1])**2+(pos[:,2]-cen[2])**2)
    
        return r
    
    
    # get mass profile for a given particle type
    def get_cum_mass_profile(self, pos, m, cen=None, range=[0,10], rscale='linear', bins=10):
        
        if cen is None: cen = [self.xc,self.yc,self.zc]
        rpart = self.get_distance_from_halo(pos, cen=cen)
        index = np.argsort(rpart)
        r_sorted = rpart[index]
        m_sorted = m[index]
        r_sorted = np.insert(r_sorted, 0, 0)
        m_sorted = np.insert(m_sorted, 0, 0)
        m_cumsum = np.cumsum(m_sorted)
        if rscale=='log':
            r = 10**np.linspace(np.log10(range[0]), np.log10(range[1]), bins+1)
        else:
            r = np.linspace(range[0], range[1], bins+1)
        m = np.interp(r, r_sorted, m_cumsum)
    
        return r, m
    
    
    # get density profile for a given particle type
    def get_density_profile(self, pos, m, cen=None, range=[0,10], rscale='log', bins=10):
        
        if cen is None: cen = [self.xc,self.yc,self.zc]
        r_edge, m_cum = self.get_cum_mass_profile(pos, m, cen=cen, range=range, rscale=rscale, bins=bins)
        dm = m_cum[1:]-m_cum[:-1]
        dV = 4*np.pi*(r_edge[1:]**3-r_edge[:-1]**3)/3
        r = 0.5*(r_edge[1:]+r_edge[:-1])
    
        return r, dm/dV
        
        
    # get temperature profile
    def get_temperature_profile(self, cen=None, range=[0,10], bins=10, rscale='linear', wt='mass'):
        
        p0 = self.sp.p0
        p0.load(block_group=('rho','phase'))
        if cen is None: cen = [self.xc,self.yc,self.zc]
        r0 = self.get_distance_from_halo(p0.p, cen=cen)
        ok = (r0>=range[0]) & (r0<range[1])
        
        weights = p0.m / p0.rho if wt in ['volume','V','Volume'] else p0.m
        if rscale=='log':
            bins = 10**np.linspace(np.log10(range[0]), np.log10(range[1]), bins+1)
        Hw, r_edges = np.histogram(r0[ok], range=range, bins=bins, weights=weights[ok])
        HwT, _ = np.histogram(r0[ok], range=range, bins=bins, weights=weights[ok]*p0.T[ok])
        HwT[Hw>0] /= Hw[Hw>0]; r = 0.5*(r_edges[1:]+r_edges[:-1])
        
        return r, HwT
        
        
    # iteratively find COM in a given aperture
    def get_COM_aperture(self, pos, m, cen=None, r=1e10, dr=1e10):
    
        import physics as phys
        from scipy.spatial import distance
        if cen is None: cen = [self.xc,self.yc,self.zc]
        
        while True:
            rp = self.get_distance_from_halo(pos, cen=cen)
            cen_new = phys.center_of_mass(pos[rp<r], m[rp<r])
            drc = distance.euclidean(cen, cen_new)
            cen = cen_new
            if (drc<=dr): break
    
        return cen
        
        
    # phase diagram
    def phase_diagram(self, cen=None, rrange=[0,10], range=[[-2,6],[1,8]], bins=[80,70], wt='mass', **kwargs):
    
        p0 = self.sp.p0
        p0.load(block_group=('rho','phase'))
        if cen is None: cen = [self.xc,self.yc,self.zc]
        r0 = self.get_distance_from_halo(p0.p, cen=cen)
        ok = (r0>=rrange[0]) & (r0<rrange[1])
        
        # calculate phase diagram
        weights = p0.m / p0.rho if wt in ['volume','V','Volume'] else p0.m
        H, _, _ = np.histogram2d(np.log10(408*p0.rho[ok]), np.log10(p0.T[ok]), weights=weights[ok], range=range, bins=bins)
        
        # prepare the plot
        if 'draw' not in kwargs: kwargs['draw'] = 1
        if 'show' not in kwargs: kwargs['show'] = 1
        if 'save' not in kwargs: kwargs['save'] = 0
        if (kwargs['draw']==1):
            import visual
            visual.plot_phase_diagram(H, range=range, bins=bins, **kwargs)
    
        return H


    # star formation history
    def get_SFH(self, dt=0.01, cum=False, cen=None, rmax=0):
        
        sp = self.sp
        p4 = self.p4
        p4.load()
        
        if (sp.cosmological):
            tnow = sp.cosmo.cosmic_time(sp.redshift)
        else:
            tnow = sp.time
        
        # crop particles outside rmax
        if cen is None: cen = [self.xc,self.yc,self.zc]
        r4 = self.get_distance_from_halo(p4.p, cen=cen)
        rmax = self.rvir if rmax<=0 else rmax
        
        return phys.get_SFH(tnow-p4.age[r4<rmax], p4.m[r4<rmax], dt=dt, cum=cum)
    
    
    # this calls my own visual module
    def viewpart(self, ptype, field=None, method='simple', coords='cart', **kwargs):

        self.load()
        if (self.k==-1): return -1 # no halo

        part = self.part[ptype]; part.load()
        if (part.k==-1): return -1 # no valid particle

        # set center and boundaries
        if 'cen' not in kwargs: kwargs['cen'] = [self.xc,self.yc,self.zc]
        if 'L' not in kwargs: kwargs['L'] = self.rvir
        
        # add time label for the image
        if 'time' not in kwargs: kwargs['time'] = r"$z=%.1f$" % self.redshift if self.cosmological else r"$t=%.1f$" %self.time
        
        # set color map
        if 'cmap' not in kwargs: kwargs['cmap'] = 'gnuplot2'
        
        # set the units
        if 'UnitMass_in_Msun' not in kwargs: kwargs['UnitMass_in_Msun'] = 1e10
        if 'UnitLength_in_pc' not in kwargs: kwargs['UnitLength_in_pc'] = 1e3
        
        # check which field to show
        h, wt = None, np.copy(part.m)
        if isinstance(field,np.ndarray): wt *= field

        """
        if (ptype==4):
            # for stars, check if we want luminosity from some band
            import colors
            if (field in colors.colors_available) and ('age' in dir(part)) and ('z' in dir(part)):
                wt *= colors.colors_table(part.age, part.z/0.02, band=field)
        """
        if (method=='smooth'):
            if ptype==0: part.load(block_group=('h',))
            h = part.h if ptype==0 else get_particle_hsml(part.p[:,0],part.p[:,1],part.p[:,2], DesNgb=10)
        
        # now, call the routine
        import visual
        if 'draw' not in kwargs: kwargs['draw'] = 1
        if 'show' not in kwargs: kwargs['show'] = 1
        if 'save' not in kwargs: kwargs['save'] = 0
        H = visual.make_projected_image(part.p, wt, h=h, method=method, coords=coords, **kwargs)
            
        return H
    

    # this calls PFH visualization module
    def image_maker(self, ptype, **kwargs):

        self.load()
        if (self.k==-1): return -1 # no halo
        
        # set center if needed
        if 'cen' not in kwargs: kwargs['cen'] = [self.xc, self.yc, self.zc]
        if 'L' not in kwargs: kwargs['L'] = self.rvir
        if 'fbase' not in kwargs: kwargs['fbase'] = "snap%03d_halo%d" %(self.sp.snum,self.id)

        import visual
        massmap = visual.image_maker(self.sp, ptype, **kwargs)
    
        return massmap


    def calculate_zoom_center(self, ptype=4):

        # the halo does not exist
        if (self.k==-1): return 0., 0., 0.

        cen, rvir = [self.xc,self.yc,self.zc], self.rvir
        part = self.loadpart(ptype)

        # particle does not exist
        if (part.k==-1): return xc, yc, zc

        # load particle coordinates
        p = part.p
        r = np.sqrt((p[:,0]-cen[0])**2+(p[:,1]-cen[1])**2+(p[:,2]-cen[2])**2)

        # there is no valid particle in rvir
        if (len(r[r<rvir])==0): return cen

        # this is the place we want to start
        cen = np.median(p[r<rvir], axis=0)
        for rmax in [50,20,10,5,2,1]:
            if (rmax>rvir): continue
            # do 5 iteration at each level
            for i in range(5):
                r = np.sqrt((p[:,0]-cen[0])**2+(p[:,1]-cen[1])**2+(p[:,2]-cen[2])**2)
                cen = np.median(p[r<rmax], axis=0)

        return cen
