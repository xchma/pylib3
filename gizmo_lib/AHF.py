import numpy as np
import os
import glob


# change this if needed
AHF_HOME = os.environ['PYLIB'] + "/gizmo_lib/AHF/"


class AHF:

    def __init__(self, sp):

        # AHF catalog may exist even if the snapshot doesn't
        self.sp = sp
        self.k = 0 if sp.cosmological else -1

        return


    def load(self, hdir=None):

        if (self.k!=0): return

        # load AHF catalog
        sp = self.sp
        if hdir is None: hdir = sp.sdir + "/AHFHalos"
        hfile = hdir + "/snap%03d*.AHF_halos"%sp.snum
        flist = glob.glob(hfile)
        
        # if not exist, leave k=0
        if (len(flist)==0): return
        hfile = flist[0]
	    
        # read the blocks
        hinv = 1.0/sp.hubble 
        ascale = sp.time
        ID, hostHalo, npart, n_gas, n_star = \
                np.loadtxt(hfile, usecols=(0,1,4,52,72,), unpack=True, dtype='int')
        Mvir, M_gas, M_star = hinv*np.loadtxt(hfile, usecols=(3,53,73,), unpack=True)
        Xc, Yc, Zc, Rvir, Rmax = ascale*hinv*np.loadtxt(hfile, usecols=(5,6,7,11,12,), unpack=True)
        VXc, VYc, VZc, Vmax = np.loadtxt(hfile, usecols=(8,9,10,16,), unpack=True) # velocity in km/s
        fMhires = np.loadtxt(hfile, usecols=(37,), unpack=True)

        # now write to class
        self.k = 1
        self.hdir = hdir
        self.nhalo = len(ID)
        self.ID = ID
        self.hostHalo = hostHalo
        self.npart = npart
        self.n_gas = n_gas
        self.n_star = n_star
        self.Mvir = Mvir
        self.M_gas = M_gas
        self.M_star = M_star
        self.Xc = Xc
        self.Yc = Yc
        self.Zc = Zc
        self.Rvir = Rvir
        self.Rmax = Rmax
        self.VXc = VXc
        self.VYc = VYc
        self.VZc = VZc
        self.Vmax = Vmax
        self.fMhires = fMhires

        return


    def runAHF(self, AHFDir=None):

        # try load AHF catalog, see if it already exists
        self.load(hdir=AHFDir)
        if (self.k!=0): return

        # no snapshot, just return
        sp = self.sp
        if (sp.k==-1): return

        # single file or multiple files
        FL = 1 if sp.nfile>1 else 0
        SnapDir = sp.sdir
        SnapNum = sp.snum

        # create converted binary files
        if (FL == 1):
            SnapFile = SnapDir + "/snapdir_%03d" % SnapNum
            SnapFile_converted = SnapDir + "/snap_converteddir_%03d" % SnapNum
            os.system("mkdir "+SnapFile_converted)
        else:
            SnapFile = SnapDir + "/snapshot_%03d.hdf5" % SnapNum
            SnapFile_converted = SnapDir + "/snap_convertedshot_%03d" % SnapNum

        # create parameter file for AHF
        if AHFDir is None: AHFDir = SnapDir + "/AHFHalos"
        if (not os.path.exists(AHFDir)): os.system("mkdir -p %s" %AHFDir)
        InputFile = AHFDir + "/AMIGA.input%03d" % SnapNum
    
        # do the actual run
        CONVERT = "python " + AHF_HOME + "HDF5converter.py " + SnapFile
        os.system(CONVERT)
        createINPUT = "python " +  AHF_HOME + "createAMIGAinput.py " + SnapDir + " " + AHFDir + " %03d %d" %(SnapNum, FL)
        os.system(createINPUT)
        RUNAHF = "AHF-v1.0-069 " + InputFile
        os.system(RUNAHF)
    
        # finally, delete the converted binary file
        os.system("rm -r "+SnapFile_converted)
    
        return
