import numpy as np
import h5py
import os
from gizmo_lib.particle import Particle
from gizmo_lib.AHF import AHF
from gizmo_lib.halo import Halo
from cosmology import Cosmology
import physics as phys


# single snapshot
class Snapshot:

    def __init__(self, sdir, snum, cosmological=0):
        
        self.sdir = sdir
        self.snum = snum
        self.cosmological = cosmological
        self.nfile = self.check_snap_exist()
        self.k = -1 if self.nfile==0 else 1
        if self.k==-1:
            print("Snapshot does not exist.")
            return
        
        # read snapshot header
        snapfile = self.get_snap_file_name(0)
        f = h5py.File(snapfile, 'r')
        h = f['Header']; keys = h.attrs.keys()
        self.npart = h.attrs['NumPart_Total']
        self.npart_this = h.attrs['NumPart_ThisFile']
        self.time = h.attrs['Time']
        self.redshift = h.attrs['Redshift']
        self.boxsize = h.attrs['BoxSize']
        if 'Omega0' in keys: self.omega = h.attrs['Omega0']
        if 'Omega_Matter' in keys: self.omega = h.attrs['Omega_Matter']
        self.hubble = h.attrs['HubbleParam']
        self.Flag_Sfr = h.attrs['Flag_Sfr']
        self.Flag_Cooling = h.attrs['Flag_Cooling']
        self.Flag_StellarAge = h.attrs['Flag_StellarAge']
        self.Flag_Metals = h.attrs['Flag_Metals']
        f.close()
        
        # correct for (non-)cosmological runs
        if (self.cosmological==0): self.time /= self.hubble
        if (self.cosmological==1): self.boxsize *= (self.time/self.hubble)
        
        # initialize particle classes
        self.p0 = Particle(self, 0)
        self.p1 = Particle(self, 1)
        self.p2 = Particle(self, 2)
        self.p3 = Particle(self, 3)
        self.p4 = Particle(self, 4)
        self.p5 = Particle(self, 5)
        self.part = [self.p0,self.p1,self.p2,self.p3,self.p4,self.p5]
        
        # AHF catalog and cosmology functions
        if (self.cosmological): self.AHF = AHF(self)
        if (self.cosmological): self.cosmo = Cosmology(self.hubble,self.omega)
        
        return
    
    
    def loadpart(self, ptype, block_group=()):
        
        part = self.part[ptype]
        part.load(block_group=block_group)
    
        return part
    
    
    def loadAHF(self, hdir=None):
        
        AHF = self.AHF
        AHF.load(hdir=hdir)
    
        return AHF
    
    
    def runAHF(self, hdir=None):
        
        AHF = self.AHF
        AHF.runAHF(AHFDir=hdir)
    
        return
    
    
    def loadhalo(self, hdir=None, hlid=0):
        
        hl = Halo(self)
        hl.load(hdir=hdir, hlid=hlid)
    
        return hl
    
    
    def get_SFH(self, dt=0.01, cum=False):
        
        p4 = self.p4
        p4.load()
        
        if (self.cosmological):
            tnow = self.cosmo.cosmic_time(1/self.time-1)
        else:
            tnow = self.time
    
        return phys.get_SFH(tnow-p4.age, p4.m, dt=dt, cum=cum)


    # number of files per snapshot
    def check_snap_exist(self):
    
        sdir = self.sdir
        snum = self.snum
        
        # single file
        snapfile = sdir + "/snapshot_%03d.hdf5" %snum
        if (os.path.isfile(snapfile)): return 1
        
        # miltiple files
        snapfile = sdir + "/snapdir_%03d/snapshot_%03d.0.hdf5" %(snum,snum)
        if (os.path.isfile(snapfile)):
            f = h5py.File(snapfile, 'r')
            nfile = f['Header'].attrs['NumFilesPerSnapshot']
            f.close()
        else:
            return 0
        
        for i in np.arange(1,nfile,1):
            snapfile = sdir + "/snapdir_%03d/snapshot_%03d.%d.hdf5" %(snum,snum,i)
            if (not os.path.isfile(snapfile)): return 0
    
        return nfile


    # file name of snapshot
    def get_snap_file_name(self, ifile):
    
        sdir = self.sdir
        snum = self.snum
        
        if (self.nfile==1):
            snapfile = sdir + "/snapshot_%03d.hdf5" %snum
        else:
            ifile = 0 if ifile>=self.nfile else ifile # protect
            snapfile = sdir + "/snapdir_%03d/snapshot_%03d.%d.hdf5" %(snum,snum,ifile)
        
        return snapfile
     
     
# initial condition
class IC:

    def __init__(self, snapfile):
    
        # read header
        f = h5py.File(snapfile, 'r')
        self.npart = f['Header'].attrs['NumPart_Total']
        self.npart_this = f['Header'].attrs['NumPart_ThisFile']
        self.boxsize = 0.0
        f.close()
        
        # aux fields
        self.k = 1
        self.nfile = 1
        self.fname = snapfile
        self.hubble = 1.0
        self.cosmological = 0
        self.time = 0.0
        self.redshift = 0.0
        
        # initialize particle classes
        self.p0 = Particle(self, 0)
        self.p1 = Particle(self, 1)
        self.p2 = Particle(self, 2)
        self.p3 = Particle(self, 3)
        self.p4 = Particle(self, 4)
        self.p5 = Particle(self, 5)
        self.part = [self.p0,self.p1,self.p2,self.p3,self.p4,self.p5]
        
        return
    
    
    def loadhalo(self):
    
        hl = Halo(self)
        hl.load()
        
        return hl
    
    
    def loadpart(self, ptype, block_group=()):
            
        part = self.part[ptype]
        part.load(block_group=block_group)
        
        return part
        
        
    # file name of snapshot
    def get_snap_file_name(self, ifile):
    
        return self.fname


# snapshot group
class SnapGroup:

    def __init__(self, sdir, snum, cosmological=1):
    
        self.sdir = sdir
        self.snum = self.tolist(snum)
        self.cosmological = cosmological
        self.snapshot = []
        for snum in self.snum:
            sp = Snapshot(sdir, snum, cosmological=cosmological)
            self.snapshot.append(sp); sp.group = self
    
        return
        
        
    # load a new group of snapshots
    def load(self, snum):
    
        for n in self.tolist(snum):
            if n in self.snum: continue
            sp = Snapshot(self.sdir, n, cosmological=self.cosmological)
            self.snapshot.append(sp); self.snum.append(n); sp.group = self
    
        return
        
        
    # find a given snapshot from a group
    def loadsnap(self, snum):
    
        if snum not in self.snum: self.load(snum)
        i = self.snum.index(snum)
    
        return self.snapshot[i]


    # find a snapshot list from a group
    def snaplist(self, snum):
    
        snum = self.tolist(snum); self.load(snum)
        
        return [self.snapshot[snum.index(n)] for n in snum]
        
        
    # convert to a list
    def tolist(self, snum):
    
        snum = np.array(snum, ndmin=1)
    
        return list(snum)
