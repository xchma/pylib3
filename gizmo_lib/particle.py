import numpy as np
import h5py
import os
import physics


class Particle:

    def __init__(self, sp, ptype):

        # no snapshot, no particle
        self.sp = sp
        self.k = -1 if sp.k==-1 else 0
        self.ptype = ptype

        return
    
    
    def load(self, block_group=()):
        
        block_list = ("Coordinates","Masses")
        
        if 'v' in block_group:
            block_list += ("Velocities",)
        if 'h' in block_group:
            block_list += ("SmoothingLength",)
        if 'z' in block_group:
            block_list += ("Metallicity",)
        if 'phase' in block_group:
            block_group += ('u','rho','nh','ne',)
        if 'u' in block_group:
            block_list += ("InternalEnergy",)
        if 'nh' in block_group:
            block_list += ("NeutralHydrogenAbundance",)
        if 'ne' in block_group:
            block_list += ("ElectronAbundance",)
        if 'id' in block_group:
            block_list += ("ParticleIDs",)
        if 'sfr' in block_group:
            block_list += ("StarFormationRate",)
        if 'rho' in block_group:
            block_list += ("Density",)
        if 'metal' in block_group:
            block_list += ("Abundances",)
        if 'B' in block_group:
            block_list += ("MagneticField",)
        if 'divB' in block_group:
            block_list += ("DivergenceOfMagneticField",)
        if 'ucr' in block_group:
            block_list += ("CosmicRayEnergy",)
        if 'photon' in block_group:
            block_list += ("PhotonEnergy",)
        if 'cs' in block_group:
            block_list += ("SoundSpeed",)
        if 'mdot' in block_group:
            block_list += ("BH_Mdot",)
        if 'mbh' in block_group:
            block_list += ("BH_Mass",)
        if 'phi' in block_group:
            block_list += ("Potential",)
        if self.ptype==4:
            block_list += ("StellarFormationTime",)

        # read the blocks
        for block_name in block_list:
            self.loadblock(block_name)

        # auxiliary blocks
        if 'phase' in block_group:
            T = physics.gas_temperature(self.u, self.ne)
            setattr(self, 'T', T)
            setattr(self, "Temperature", T)
        if self.ptype==4:
            age = self.get_stellar_ages(self.sft)
            setattr(self, 'age', age)
            setattr(self, "StellarAges", age)
    
        return
    
    
    def loadblock(self, block_name):
        
        # snapshot doesn't exist
        if (self.k==-1): return
        
        # block already loaded
        if block_name in dir(self): return
        
        # class basic info
        sp = self.sp
        ptype = self.ptype
        npart = sp.npart[ptype]
        
        # no particle in this ptype
        if (npart==0): return
        
        # pre-factor for converting units
        hinv = 1.0 / sp.hubble
        ascale = sp.time if sp.cosmological else 1.0
        
        # allocate particle array
        if block_name == "Coordinates":
            d = np.zeros((npart,3), dtype='float64')
        elif block_name in ["Velocities","MagneticField","PhotonEnergy"]:
            d = np.zeros((npart,3), dtype='float32')
        elif block_name == "Masses":
            d = np.zeros(npart, dtype='float64')
        elif block_name == "ParticleIDs":
            d = np.zeros(npart, dtype=np.dtype('uint32'))
        elif block_name == "Abundances":
            d = np.zeros((npart,sp.Flag_Metals), dtype='float32')
        else:
            d = np.zeros(npart, dtype='float32')

        # do the reading
        nL = 0
        for ifile in range(sp.nfile):
            snapfile = sp.get_snap_file_name(ifile)
            f = h5py.File(snapfile, 'r')
            nR = nL + f['Header'].attrs['NumPart_ThisFile'][ptype]
            g = f['PartType%d'%ptype]
            if (block_name=="Metallicity"):
                d[nL:nR] = g['Metallicity'][:,0]
            elif (block_name=="Abundances"):
                d[nL:nR] = g['Metallicity'][...]
            else:
                d[nL:nR] = g[block_name][...]
            f.close()
            nL = nR
        
        # convert units
        if ((block_name=="Coordinates")|(block_name=="SmoothingLength")):
            d *= (ascale*hinv)
        if (block_name=="Velocities"):
            d *= np.sqrt(ascale)
        if (block_name=="Masses"):
            d *= hinv
        if (block_name=="Density"):
            d *= (hinv/(ascale*hinv)**3)
        if (block_name=="StellarFormationTime"):
            if (sp.cosmological==0): d *= hinv
        
        # add attribute to class
        if (block_name=="Coordinates"):
            attr_name = 'p'
        elif (block_name=="Velocities"):
            attr_name = 'v'
        elif (block_name=="Masses"):
            attr_name = 'm'
        elif (block_name=="ParticleIDs"):
            attr_name = 'id'
        elif (block_name=="SmoothingLength"):
            attr_name = 'h'
        elif (block_name=="InternalEnergy"):
            attr_name = 'u'
        elif (block_name=="StarFormationRate"):
            attr_name = 'sfr'
        elif (block_name=="Density"):
            attr_name = 'rho'
        elif (block_name=="NeutralHydrogenAbundance"):
            attr_name = 'nh'
        elif (block_name=="ElectronAbundance"):
            attr_name = 'ne'
        elif (block_name=="StellarFormationTime"):
            attr_name = 'sft'
        elif (block_name=="Metallicity"):
            attr_name = 'z'
        elif (block_name=="Abundances"):
            attr_name = 'z'
        elif (block_name=="MagneticField"):
            attr_name = 'B'
        elif (block_name=="DivergenceOfMagneticField"):
            attr_name = 'divB'
        elif (block_name=="CosmicRayEnergy"):
            attr_name = 'ucr'
        elif (block_name=="PhotonEnergy"):
            attr_name = 'photon'
        elif (block_name=="SoundSpeed"):
            attr_name = 'cs'
        elif (block_name=="BH_Mdot"):
            attr_name = 'mdot'
        elif (block_name=="BH_Mass"):
            attr_name = 'mbh'
        elif (block_name=="Potential"):
            attr_name = 'phi'
        else:
            attr_name = block_name
        
        setattr(self, attr_name, d)
        setattr(self, block_name, d)
        setattr(self, 'k', 1)

        return


    # get stellar age from SF time
    def get_stellar_ages(self, sft):
        
        sp = self.sp
        if (sp.cosmological):
            t_form = sp.cosmo.cosmic_time(1/sft-1)
            t_now = sp.cosmo.cosmic_time(1/sp.time-1)
            age = t_now - t_form # in Gyr
        else:
            age = sp.time - sft # code already in Gyr
        
        return age
