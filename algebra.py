import numpy as np

# convert a radial vector to angular coordinates
def RadialVector2AngularCoordiante(nx, ny, nz):
    
    norm = np.sqrt(nx*nx+ny*ny+nz*nz)
    if (norm==0.):
        theta, phi = 0., 0.
    else:
        nx /= norm; ny /= norm; nz /= norm
        if (nz==1.):
            theta, phi = 0., 0.
        elif (nz==-1.):
            theta, phi = np.pi, np.pi
        else:
            theta = np.arccos(nz)
            phi = np.arccos(nx/np.sin(theta))
            if (ny < 0):
                phi = 2*np.pi - phi

    return theta, phi


# coordinate transform via rotation
def CoordinatesRotate(x, y, z, theta, phi, psi):
    # 1. rotate phi along +z to x'y'z' frame
    # 2. rotate theta along +y' to x"y"z" frame
    # 3. rotate psi along +z" to final frame
    # Example: theta=pi/2, phi=0, psi=pi/2 -- yz projection
    #          theta=pi/2, phi=pi/2, psi=pi -- zx projection
    
    A11 = np.cos(theta)*np.cos(phi)*np.cos(psi) - np.sin(phi)*np.sin(psi)
    A12 = np.cos(theta)*np.sin(phi)*np.cos(psi) + np.cos(phi)*np.sin(psi)
    A13 = -np.sin(theta)*np.cos(psi)
    A21 = -np.cos(theta)*np.cos(phi)*np.sin(psi) - np.sin(phi)*np.cos(psi)
    A22 = -np.cos(theta)*np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)
    A23 = np.sin(theta)*np.sin(psi)
    A31 = np.sin(theta)*np.cos(phi)
    A32 = np.sin(theta)*np.sin(phi)
    A33 = np.cos(theta)
    
    x_new = A11*x + A12*y + A13*z
    y_new = A21*x + A22*y + A23*z
    z_new = A31*x + A32*y + A33*z
    
    return x_new, y_new, z_new


# coordinate transform via rotation
def CoordinatesRotateInverse(x, y, z, theta, phi, psi):
    
    A11 = np.cos(theta)*np.cos(phi)*np.cos(psi) - np.sin(phi)*np.sin(psi)
    A12 = -np.cos(theta)*np.cos(phi)*np.sin(psi) - np.sin(phi)*np.cos(psi)
    A13 = np.sin(theta)*np.cos(phi)
    A21 = np.cos(theta)*np.sin(phi)*np.cos(psi) + np.cos(phi)*np.sin(psi)
    A22 = -np.cos(theta)*np.sin(phi)*np.sin(psi) + np.cos(phi)*np.cos(psi)
    A23 = np.sin(theta)*np.sin(phi)
    A31 = -np.sin(theta)*np.cos(psi)
    A32 = np.sin(theta)*np.sin(psi)
    A33 = np.cos(theta)
    
    x_old = A11*x + A12*y + A13*z
    y_old = A21*x + A22*y + A23*z
    z_old = A31*x + A32*y + A33*z
    
    return x_old, y_old, z_old
    
    
# vector algebra
def dot(a, b):

    return a[:,0]*b[:,0] + a[:,1]*b[:,1] + a[:,2]*b[:,2]
    
def cross(a, b):

    c = np.zeros(a.shape)
    c[:,0] = a[:,1]*b[:,2] - a[:,2]*b[:,1]
    c[:,1] = a[:,2]*b[:,0] - a[:,0]*b[:,2]
    c[:,2] = a[:,0]*b[:,1] - a[:,1]*b[:,0]
    
    return c


# calculate percentile for a weighted array
# q must be an array in [0,1]
def WeightedPercentile(x, q, weights=None):
    
    # assign equal weights if empty
    if weights is None: weights = np.ones(len(x))
    
    # need to sort the array
    index = np.argsort(x)
    x_sorted, w_sorted = x[index], weights[index]
    
    # get the cumulative weights
    w_cum = np.cumsum(w_sorted)
    w_cum /= w_cum[-1]

    return np.interp(np.array(q), w_cum, x_sorted)


# two-dimensional percentile calculation for a
# weighted array, q must be an array in [0,1]
def WeightedPercentile2D(x, y, q, weights=None, range=None, bins=50, dx=0.2, Nmin=10):
    
    # assign equal weights if empty
    if weights is None: weights = np.ones(len(x))
    
    # get the x-range to do statistics
    try:
        xmin, xmax = range[0], range[1]
    except TypeError:
        xmin, xmax = np.min(x), np.max(x)

    # sort the array
    index = np.argsort(x)
    x_sorted, y_sorted, w_sorted = x[index], y[index], weights[index]

    # allocate output
    xmid = np.linspace(xmin, xmax, bins+2)[1:-1]
    xout = np.zeros(bins)
    yout = np.zeros((bins,len(q)))

    # loop over bins
    for i in np.arange(bins):
        xleft, xright = xmid[i]-dx/2, xmid[i]+dx/2
        ileft, imid, iright = tuple(np.searchsorted(x_sorted, (xleft, xmid[i], xright)))
        if (iright-ileft<Nmin):
            ileft = max(0, imid-Nmin/2)
            iright = min(len(x_sorted), imid+Nmin/2)
        xout[i] = np.median(x_sorted[ileft:iright])
        yout[i,:] = WeightedPercentile(y_sorted[ileft:iright], q, weights=w_sorted[ileft:iright])

    return xout, yout


# two-dimensional statistics
def BinnedStatistics(xdata, ydata, bins=50, range=None, dx=0.2, Nmin=10):
    
    # get the x-range of the statistics
    try:
        xmin, xmax = range[0], range[1]
    except TypeError:
        xmin, xmax = np.min(xdata), np.max(xdata)

    # sort the x array
    index = np.argsort(xdata)
    x_sorted, y_sorted = xdata[index], ydata[index]

    # allocate output
    xmid = np.linspace(xmin, xmax, bins+2)[1:-1]
    xout, yout = np.zeros(bins), np.zeros(bins)
    ylo, yhi = np.zeros(bins), np.zeros(bins)

    # loop over bins
    for i in np.arange(bins):
        xleft, xright = xmid[i]-dx/2, xmid[i]+dx/2
        ileft = np.searchsorted(x_sorted, xleft)
        imid = np.searchsorted(x_sorted, xmid[i])
        iright = np.searchsorted(x_sorted, xright)
        # make sure there are Nmin data in each bin
        if (iright-ileft<Nmin):
            ileft = max(0, imid-Nmin/2)
            iright = min(len(x_sorted), imid+Nmin/2)
        # 1-sigma range in each bin
        xout[i] = np.median(x_sorted[ileft:iright])
        yout[i] = np.median(y_sorted[ileft:iright])
        ylo[i] = np.percentile(y_sorted[ileft:iright], 16)
        yhi[i] = np.percentile(y_sorted[ileft:iright], 84)

    return xout, ylo, yhi


# weighted center for a structure
def ImageWeightedCenter(x, y, value=None):
    
    # not given, do geometric center
    if value is None: value = np.ones(len(x))

    M00 = np.sum(value)
    M10 = np.sum(x*value)
    M01 = np.sum(y*value)

    return M10/M00, M01/M00


# weighted size for a structure - see wikipedia image moment page
def ImageWeightedSize(x, y, value=None):
    
    # not given, do geometric center
    if value is None: value = np.ones(len(x))
    if len(x)==1: return 0.0, 0.0

    M00 = np.sum(value)
    M10 = np.sum(x*value)
    M01 = np.sum(y*value)
    M20 = np.sum(x*x*value)
    M02 = np.sum(y*y*value)
    M11 = np.sum(x*y*value)
    xbar = M10/M00
    ybar = M01/M00

    mu20 = M20/M00 - xbar*xbar
    mu02 = M02/M00 - ybar*ybar
    mu11 = M11/M00 - xbar*ybar

    Delta = 4*mu11*mu11 + (mu20-mu02)*(mu20-mu02)
    major_sigma = np.sqrt((mu20+mu02+np.sqrt(Delta))/2)
    if mu20+mu02-np.sqrt(Delta)<=0:
        minor_sigma = 0.0
    else:
        minor_sigma = np.sqrt((mu20+mu02-np.sqrt(Delta))/2)

    return major_sigma, minor_sigma


# get the cumulative distribution of an array
def CumulativeDistribution(x, descending=True):
    
    x_sorted = x[np.argsort(x)]
    if descending: x_sorted = x_sorted[::-1]
    x_cum = np.cumsum(x_sorted)

    return x_sorted, x_cum/x_cum[-1]


# find a fractional cumulative value in an array
def FractionalCumValue(x, q=(0.5,), descending=True, rank=False):
    
    x_sorted, y_frac = CumulativeDistribution(x, descending=descending)
    if rank: x_sorted = 1 + np.arange(len(x))
    x_frac_list = () # need a tuple
    for q_frac in q:
        x_frac = np.interp(q_frac, y_frac, x_sorted)
        x_frac_list += (x_frac,)

    return x_frac_list


# compute Gimi coefficient
def GiniCoefficient(x):
    
    n = len(x)
    i = np.arange(1,n+1,1)
    x_bar = np.mean(x)
    x_sorted = x[np.argsort(x)]
    
    g = (2*i-n-1)*x_sorted
    G = np.sum(g)/x_bar/n/(n-1)

    return G


# compute M20 coefficient
def M20Coefficient(x, y, value=None):
    
    if value is None: value = np.ones(len(x))
    xc, yc = ImageWeightedCenter(x, y, value=value)
    
    Mtot = np.sum(value*((x-xc)**2+(y-yc)**2))
    v20, = FractionalCumValue(value, q=(0.8,))
    SumM = np.sum(value[value>v20]*((x[value>v20]-xc)**2+(y[value>v20]-yc)**2))

    return np.log10(SumM/Mtot)
