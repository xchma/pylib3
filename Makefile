SYSTYPE="Apple"
#SYSTYPE="TACC"

ifeq ($(SYSTYPE),"Apple")
export HDF5_PATH=/usr/local/hdf5
else ifeq ($(SYSTYPE),"TACC")
export HDF5_PATH=$(TACC_HDF5_PATH)
endif

all:
	cd c_library/deposit; make; cd ../..
	cd c_library/gaslocal; make; cd ../..
	cd c_library/interp; make; cd ../..
	cd c_library/octree; make; cd ../..
	cd c_library/starhsml; make; cd ../..
	cd c_library/tools; make; cd ../..
	cd sps/bpass/synthesis; make; cd ../../..

clean:
	rm -rf __pycache__ */__pycache__ */*/__pycache__ */*/*/__pycache__
	cd c_library/deposit; make clean; cd ../..
	cd c_library/gaslocal; make clean; cd ../..
	cd c_library/interp; make clean; cd ../..
	cd c_library/octree; make clean; cd ../..
	cd c_library/starhsml; make clean; cd ../..
	cd c_library/tools; make clean; cd ../..
	cd sps/bpass/synthesis; make clean; cd ../../..
