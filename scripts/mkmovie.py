#!/usr/bin/env python

import argparse
import sys, os

parser = argparse.ArgumentParser(description="ffmpeg options")
parser.add_argument('infile', type=str, help="Input file name")
parser.add_argument('--frate', '-r', type=int, default=6, help="Frame rate")
parser.add_argument('--start', '-s', type=int, default=0, help="Start number")
parser.add_argument('--ofile', '-o', type=str, default="movie.mp4", help="Output file name")
args = parser.parse_args()
infile, frate, start, ofile = args.infile, args.frate, args.start, args.ofile

command = "ffmpeg -framerate %d -start_number %d -i %s -vb 20M -vcodec mpeg4 %s"%(frate,start,infile,ofile)
#print(command)
os.system(command)
