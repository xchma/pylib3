#!/usr/bin/env python

import argparse
import numpy as np
import h5py
import glob
import os
import gc

parser = argparse.ArgumentParser(description="Clip snapshot file")
parser.add_argument('sdir', type=str, help="Snapshot directory")
parser.add_argument('snum', type=int, help="Snapshot number")
parser.add_argument('--hlid', type=int, default=0, help="Halo ID")
parser.add_argument('--fvir', type=float, default=1.2, help="Fractional virial radius to keep")
parser.add_argument('--rmax', type=float, default=1e3, help="Maximum physical radius to keep")
parser.add_argument('--odir', '-o', type=str, default="clipped", help="Output directory")
args = parser.parse_args()
sdir, snum, hlid, fvir, rmax = args.sdir, args.snum, args.hlid, args.fvir, args.rmax
odir = sdir + "/%s"%args.odir # relative path

# create output directory if not exist
if not os.path.exists(odir): os.system("mkdir %s"%odir)

# check if file exist
fname = sdir + "/snapshot_%03d.hdf5"%snum
if not os.path.isfile(fname): fname = sdir + "/snapdir_%03d/snapshot_%03d.0.hdf5"%(snum,snum)
if not os.path.isfile(fname): print("Snapshot does not exist."); exit()

# get the header
f = h5py.File(fname, 'r')
nfile = f['Header'].attrs['NumFilesPerSnapshot']
npart_total = f['Header'].attrs['NumPart_Total']
nmetal = f['Header'].attrs['Flag_Metals']
afactor = f['Header'].attrs['Time']

# create the output file
fname_clipped = odir + "/snapshot_%03d.hdf5"%snum
fclipped = h5py.File(fname_clipped, 'w')
npart_total_clipped = np.zeros(6, dtype=np.dtype('uint32'))

# copy the header over
gclipped = fclipped.create_group('Header')
for item in f['Header'].attrs.keys():
    if item in ['NumPart_ThisFile','NumPart_Total']: continue
    data = f['Header'].attrs[item]
    gclipped.attrs[item] = data

# get the halo center and virial radius
hfile = sdir + "/AHFHalos/snap%03d*.AHF_halos"%snum
flist = glob.glob(hfile)
if (len(flist)==0): print("No halo catalog."); exit()
hfile = flist[0]
Xc, Yc, Zc, Rvir = np.loadtxt(hfile, usecols=(5,6,7,11), unpack=True)
xc, yc, zc, rvir = Xc[hlid], Yc[hlid], Zc[hlid], Rvir[hlid]

# copy the particles over
for ptype in [0,1,4]:
    # get the particle positions
    pos = np.zeros((npart_total[ptype],3), dtype='float64')
    nL = 0
    for ifile in range(nfile):
        fname = sdir + "/snapshot_%03d.hdf5"%snum if nfile==1 else sdir + "/snapdir_%03d/snapshot_%03d.%d.hdf5"%(snum,snum,ifile)
        fin = h5py.File(fname, 'r')
        npart_this = fin['Header'].attrs['NumPart_ThisFile']
        if npart_this[ptype]==0: continue
        nR = nL + npart_this[ptype]
        pos[nL:nR] = fin['PartType%d/Coordinates'%ptype][...]
        fin.close()
        nL = nR
        
    # cut the particles
    rcut = min(fvir*rvir, rmax/afactor)
    ok = np.sqrt((pos[:,0]-xc)**2+(pos[:,1]-yc)**2+(pos[:,2]-zc)**2) < rcut
    gclipped = fclipped.create_group('PartType%d'%ptype)
    gclipped.create_dataset("Coordinates", data=pos[ok])
    npart_total_clipped[ptype] = len(pos[ok])

    # port over datasets
    for item in f['PartType%d'%ptype].keys():
        # create blank arrays
        if item in ['Coordinates','ParticleChildIDsNumber','ParticleIDGenerationNumber']: continue
        if item in ['Velocities','PhotonEnergy']:
            data = np.zeros((npart_total[ptype],3), dtype='float32')
        elif item == "ParticleIDs":
            data = np.zeros(npart_total[ptype], dtype=np.dtype('uint32'))
        elif item == "Metallicity":
            data = np.zeros((npart_total[ptype], nmetal), dtype='float32')
        else:
            data = np.zeros(npart_total[ptype], dtype='float32')

        # read in data
        nL = 0
        for ifile in range(nfile):
            fname = sdir + "/snapshot_%03d.hdf5"%snum if nfile==1 else sdir + "/snapdir_%03d/snapshot_%03d.%d.hdf5"%(snum,snum,ifile)
            fin = h5py.File(fname, 'r')
            npart_this = fin['Header'].attrs['NumPart_ThisFile']
            if npart_this[ptype]==0: continue
            nR = nL + npart_this[ptype]
            data[nL:nR] = fin["PartType%d/%s"%(ptype,item)][...]
            fin.close()
            nL = nR

        # write to output
        fclipped.create_dataset("PartType%d/%s"%(ptype,item), data=data[ok])

# write particle number attribute
fclipped['Header'].attrs['NumPart_ThisFile'] = npart_total_clipped
fclipped['Header'].attrs['NumPart_Total'] = npart_total_clipped

f.close()
fclipped.close()

# clean up
del data, pos
gc.collect()
