#!/bin/bash

run=$1

rm -rf $run/{*bw*,cpu*,ewald*,gizmo*,HII*,Mom*,sfr.txt,SNe*,slurm*,cont}
rm -rf $run/{parameters-usedvalues,params.txt-usedvalues,restartfiles}
rm -rf $run/{balance.txt,blackhole*,info.txt,energy.txt,tim*txt}
rm -rf $run/AHFHalos/*{log,parameter,particles,profiles,substructure}
rm -rf $run/AHFHalos/AMIGA*
cd $run/code
make clean
