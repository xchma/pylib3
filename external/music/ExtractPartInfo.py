import numpy as np
import tools
import h5py
import glob
import os
import sys

try:
    sdir = sys.argv[1]
    snum = int(sys.argv[2])
    hlid = int(sys.argv[3])
    print("Extracting particle info")
    print("   ... run: %s, snapshot %03d, halo %d"%(run,snum,hlid))
except (IndexError, ValueError):
    print("Usage: python ExtractPartInfo.py run snum hlid [fvir fname]")
    print("    sdir - where are the snapshots?")
    print("    snum - which snapshot are you looking for?")
    print("    hlid - which halo are you looking for?")
    print("    fvir (optional) - out to how many virial radii you want to include?")
    print("    fname (optional) - output file name")
    exit()

try:
    fvir = float(sys.argv[3])
    print("    ... out to %.1f virial radius (radii)"%fvir)
except (IndexError, ValueError):
    fvir = 3.0
    print("    ... by default, include out to 3.0 virial radii")
    
try:
    fname = sys.argv[4]
    print("    ... write output to file %s"%fname)
except (IndexError, ValueError):
    fvir = "output.txt"
    print("    ... by default, write output to output.txt")

# find the snapshot
try:
    f = h5py.File("%s/snapshot_%03d.hdf5"%(sdir,snum), 'r')
except IOError:
    try:
        f = h5py.File("%s/snapdir_%03d/snapshot_%03d.0.hdf5"%(sdir,snum,snum), 'r')
    except IOError:
        print("Error: snapshot does not exist")
        exit()
red = f['Header'].attrs['Redshift']
nsnap = f['Header'].attrs['NumFilesPerSnapshot']
boxsize = f['Header'].attrs['BoxSize']
f.close()
print("    ... Redshift %.1f, %d file(s) per snapshot"%(red,nsnap))

# find halo catalog
print("Loading halo catalog...")
flist = glob.glob("%s/AHFHalos/snap%03dRPep*AHF_halos"%(sdir,snum))
if (len(flist) != 1):
    print("Error: %d file found, expect 1."%(len(flist)))
    exit()
else:
    ID, host, npart = np.loadtxt(flist[0], usecols=(0,1,4,), unpack=True, dtype='int')
    Mvir, Xc, Yc, Zc, Rvir = np.loadtxt(flist[0], usecols=(3,5,6,7,11,), unpack=True)
    mvir, xc, yc, zc, rvir = Mvir[hlid], Xc[hlid], Yc[hlid], Zc[hlid], Rvir[hlid]
    print("    ... halo catalog loaded, total %d halos"%len(Xc))
    print("    ... halo %d, virial mass %.3e, virial radius %.3f ckpc"%(hlid,mvir,rvir))

# find particle IDs from destinate snapshot
print("Searching for particles in the zoom-in region in snapshot %d..."%snum)
print("    ... create a temporary file")
fout = open("%s/tmp.txt"%sdir, 'w')
for i in range(nsnap):
    print("    ... search in file %d, total %d files"%(i,nsnap))
    try:
        if (nsnap>1):
            f = h5py.File("%s/snapdir_%03d/snapshot_%03d.%d.hdf5" %(sdir,snum,snum,i), 'r')
        else:
            f = h5py.File("%s/snapshot_%03d.hdf5" %(sdir,snum), 'r')
    except IOError:
        print("Error: snapshot is not complete")
        exit()
    npart = f['Header'].attrs['NumPart_ThisFile']
    pos = f['PartType1']['Coordinates'][...]
    pid = f['PartType1']['ParticleIDs'][...]
    
    pos[:,0] -= xc; pos[:,1] -= yc; pos[:,2] -= zc
    pos += boxsize/2; pos %= boxsize; pos -= boxsize/2
    rad = np.sqrt(pos[:,0]**2+pos[:,1]**2+pos[:,2]**2)
    plist, = np.where(rad<fvir*rvir) # cut the radius
    
    print("        --- particles identified, write the IDs to the temporary file")
    for j in plist:
        fout.write("%d\n"%(pid[j]))
    print("        --- particle info written to file")
    
    if (npart[2]==0): continue # no low-res particles
    print("        --- also include low-res particles")
    pos = f['PartType2']['Coordinates'][...]
    pid = f['PartType2']['ParticleIDs'][...]
    
    pos[:,0] -= xc; pos[:,1] -= yc; pos[:,2] -= zc
    pos += boxsize/2; pos %= boxsize; pos -= boxsize/2
    rad = np.sqrt(pos[:,0]**2+pos[:,1]**2+pos[:,2]**2)
    plist, = np.where(rad<fvir*rvir) # cut the radius
    
    for j in plist:
        fout.write("%d\n"%(pid[j]))
        
    f.close()
    
fout.close()


# trace back in snapshot 000, first, check if the snapshot exists
# find the snapshot
print("Tracing back for these particles in snapshot 000")
try:
    f = h5py.File("%s/snapshot_000.hdf5"%sdir, 'r')
except IOError:
    try:
        f = h5py.File("%s/snapdir_000/snapshot_000.0.hdf5"%sdir, 'r')
    except IOError:
        print("Error: snapshot does not exist")
        exit()
boxsize = f['Header'].attrs['BoxSize']
nsnap = f['Header'].attrs['NumFilesPerSnapshot']
f.close()
print("    ... This is snapshot 000, boxsize %.3f cMpc, %d file(s) per snapshot"%(boxsize/1e3,nsnap))

# find particle IDs from destinate snapshot
print("    ... create the output file")
PartID = np.loadtxt("%s/tmp.txt"%sdir, usecols=(0,), unpack=True, dtype='int')
fout = open("%s/%s"%(sdir,fname), 'w')
for i in range(nsnap):
    print("    ... search in file %d, total %d files"%(i,nsnap))
    try:
        if (nsnap>1):
            f = h5py.File("%s/snapdir_000/snapshot_000.%d.hdf5"%(sdir,i), 'r')
        else: 
            f = h5py.File("%s/snapshot_000.hdf5"%sdir, 'r')
    except IOError:
        print("Error: snapshot is not complete")
        exit()
    npart = f['Header'].attrs['NumPart_ThisFile']
    pos = f['PartType1']['Coordinates'][...]/boxsize
    pid = f['PartType1']['ParticleIDs'][...]

    suba, subb = tools.match(PartID, pid)
    print("        --- particles matched, write the positions to the file")
    
    for j in subb:
        fout.write("%f %f %f\n"%(pos[j,0],pos[j,1],pos[j,2]))
    print("        --- particle info written to file")
    
    if (npart[2]==0): continue
    pos = f['PartType2']['Coordinates'][...]/boxsize
    pid = f['PartType2']['ParticleIDs'][...]
    
    suba, subb = tools.match(PartID, pid)
    for j in subb:
        fout.write("%f %f %f\n"%(pos[j,0],pos[j,1],pos[j,2]))
    
    f.close()

fout.close()

os.system("rm %s/tmp.txt"%sdir)
print("Temporary file deleted.")
print("Finished.")
