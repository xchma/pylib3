#!/usr/bin/env python

import argparse
import numpy as np
from gizmo import *
import h5py

parser = argparse.ArgumentParser(description="Convert cosmological snapshot")
parser.add_argument('sdir', type=str, help="Snapshot directory")
parser.add_argument('snum', type=int, help="Snapshot number")
parser.add_argument('--hlid', type=int, default=0, help="Halo ID in Amiga Halo Catalog")
parser.add_argument('--tmin', type=float, default=0.0, help="Earliest star formation time")
parser.add_argument('--rmax', type=float, default=10.0, help="Maximum radius to include")
parser.add_argument('--odir', '-o', type=str, default=".", help="Output directory")
args = parser.parse_args()
sdir, snum, hlid = args.sdir, args.snum, args.hlid
tmin, rmax, odir = args.tmin, args.rmax, args.odir

# get the halo
sp = loadsnap(sdir, snum, cosmological=1)
hl = sp.loadhalo(hlid=hlid)

# cut the stars
p4 = sp.loadpart(4, block_group=('v','id','z'))
p4.p[:,0] -= hl.xc; p4.p[:,1] -= hl.yc; p4.p[:,2] -= hl.zc
r4 = np.sqrt(p4.p[:,0]**2+p4.p[:,1]**2+p4.p[:,2]**2)
ok = (r4<rmax) & (p4.sft>tmin)

# write the output file
fname = odir + "/snapshot_%03d.hdf5"%snum
f = h5py.File(fname, 'w')
p = f.create_group("PartType4")
p.create_dataset("Coordinates", data=p4.p[ok])
p.create_dataset("Velocities", data=p4.v[ok])
p.create_dataset("Masses", data=p4.m[ok])
p.create_dataset("ParticleIDs", data=p4.id[ok])
p.create_dataset("StarFormationTime", data=p4.sft[ok])
p.create_dataset("Metallicity", data=p4.z[ok])
f.close()
