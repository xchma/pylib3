#!/usr/bin/env python

import argparse
import numpy as np
import h5py

parser = argparse.ArgumentParser(description="Compress COLT output")
parser.add_argument('infile', type=str, help="Input file name")
parser.add_argument('--ofile', '-o', type=str, default="output.hdf5", help="Output file name")
args = parser.parse_args()
infile, ofile = args.infile, args.ofile


f = h5py.File(infile, 'r')
fo = h5py.File(ofile, 'w')

# copy attributes
for item in f.attrs.keys():
    data = f.attrs[item]
    fo.attrs[item] = data
    
# copy datasets
photon = True
for item in f.keys():
    if item in ['line','exit','sources','dust']:
        photon = False # it's not a photon file
        continue # these are groups
    data = f[item][...]
    fo.create_dataset(item, data=data, compression=1)
    for att in f[item].attrs.keys(): # dataset units
        data = f[item].attrs[att]
        fo[item].attrs[att] = data
        
if (photon==True):
    f.close()
    fo.close()
    exit() # it's a photon file
    
# copy groups
for grp in ['line','exit','sources','dust']:
    g = f[grp]
    go = fo.create_group(grp)
    # copy attributes
    for item in g.attrs.keys():
        data = g.attrs[item]
        go.attrs[item] = data
        
    # copy datasets
    for item in g.keys():
        data = g[item][...]
        go.create_dataset(item, data=data, compression=1)
        for att in g[item].attrs.keys():
            data = g[item].attrs[att]
            go[item].attrs[att] = data

f.close()
fo.close()
