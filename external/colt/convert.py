#!/usr/bin/env python

import argparse
import numpy as np
import sps.bpass
import h5py, os

# units
Msun = 1.989e33
kpc = 3.085678e21
km = 1e5

parser = argparse.ArgumentParser(description="Convert octree to COLT input")
parser.add_argument('root', type=str, help="Octree file name, without .hdf5")
parser.add_argument('--sdir', '-s', type=str, default="", help="Source and state file directory, relative path to root")
parser.add_argument('--ofile', '-o', type=str, default="output.hdf5", help="Output file name, absolute path")
args = parser.parse_args()
root, sdir, ofile = args.root, args.sdir, args.ofile


f = h5py.File(ofile, 'w')

f.attrs['redshift'] = 0 # this is not relevant

tfile = root + ".hdf5" # octree file name
if not os.path.isfile(tfile): tfile = root + ".h5"
ft = h5py.File(tfile, 'r')
Ncell = ft.attrs['Ncell']
sub_cell_check = ft['sub_cell_check'][...]
f.attrs["n_cells"] = np.int32(Ncell)
f.create_dataset("parent", data=np.array(ft['parent_ID'][...], dtype=np.int32), compression=1)
f.create_dataset("child_check", data=np.array(ft['sub_cell_check'][...], dtype=np.int32), compression=1)

sub_cell_IDs = -1*np.ones((Ncell,8), dtype=np.int32)
sub_cell_IDs[sub_cell_check==1] = ft['sub_cell_IDs'][...]
f.create_dataset("child", data=sub_cell_IDs, compression=1)

f.create_dataset("r", data=np.array(ft['min_x'][...]*kpc, dtype=np.float64), compression=1)
f["r"].attrs["units"] = b"cm"
f.create_dataset("w", data=np.array(ft['width'][...]*kpc, dtype=np.float64), compression=1)
f["w"].attrs["units"] = b"cm"

T = np.zeros(Ncell, dtype=np.float64)
T[sub_cell_check==0] = ft['T'][...]
f.create_dataset("T", data=T, compression=1)
f["T"].attrs["units"] = b"K"

z = np.zeros(Ncell, dtype=np.float64)
z[sub_cell_check==0] = ft['z'][...]
f.create_dataset("Z", data=z, compression=1)

rho = np.zeros(Ncell, dtype=np.float64)
rho[sub_cell_check==0] = ft['rho'][...]*(1e10*Msun/kpc**3)
f.create_dataset("rho", data=rho, compression=1)
f["rho"].attrs["units"] = b"g/cm^3"

v = np.zeros((Ncell,3), dtype=np.float64)
v[sub_cell_check==0] = ft['vel'][...]*km
f.create_dataset("v", data=v, compression=1)
f["v"].attrs["units"] = b"cm/s"
ft.close()

stfile = root + sdir + "/state.hdf5" # state file
if not os.path.isfile(stfile): stfile = root + sdir + "/state.h5"
fst = h5py.File(stfile, 'r')

nh = np.zeros(Ncell, dtype=np.float64)
nh[sub_cell_check==0] = fst['nh'][...]
f.create_dataset("x_HI", data=nh, compression=1)

Jion = np.zeros(Ncell, dtype=np.float64)
Jion[sub_cell_check==0] = fst['JUV'][...]
f.create_dataset("Jion", data=Jion, compression=1)
fst.close()

sfile = root + sdir + "/stars.hdf5" # source file
if not os.path.isfile(sfile): sfile = root + sdir + "/stars.h5"
fs = h5py.File(sfile, 'r')
f.create_dataset("r_star", data=np.array(fs['pos'][...]*kpc, dtype=np.float64), compression=1)
f["r_star"].attrs["units"] = b"cm"
age = fs['age'][...]
m = fs['m'][...]
z = 0.001*np.ones(m.size)
Luv = (1e10*m)*10**sps.bpass.compute_stellar_luminosity(age, z, band='Lya', binary=False)
f.create_dataset("L_UV", data=np.array(Luv, dtype=np.float64), compression=1)
f.attrs["n_stars"] = np.int32(Luv.shape)
f["L_UV"].attrs["units"] = b"erg/s/angstrom"
fs.close()

f.close()
