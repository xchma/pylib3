# This module handles colt output

import numpy as np
import h5py

kpc = 3.085678e21
w0 = 1215.67 # in AA
c = 2.99792458e5 # in km/s

class Image:

    def __init__(self, fname):
    
        f = h5py.File(fname, 'r')
        # camera axis
        self.ncam = f.attrs["n_cameras"]
        self.cdir = f["camera_directions"][...]
        self.cxdir = f["camera_xaxes"][...]
        self.cydir = f["camera_yaxes"][...]
        # frequency range
        self.nnu = f.attrs["n_bins"] # in km/s
        self.nu_min = f.attrs["freq_min"]
        self.nu_max = f.attrs["freq_max"]
        self.dnu = f.attrs["freq_bin_width"]
        # image spatial range
        self.nx = f.attrs["n_pixels"]
        self.L = f.attrs["image_radius"] / kpc
        self.dx = f.attrs["pixel_width"] / kpc
        self.fluxes = f["fluxes"][...]
        self.images = f["images"][...]
        self.fname = fname
        f.close()
        
        # coordinates and frequencies
        x = np.arange(-self.L+self.dx/2, self.L, self.dx)
        self.y, self.x = np.meshgrid(x,x) # x,y coordinates
        self.r = np.sqrt(self.x**2+self.y**2)
        self.nu = np.arange(self.nu_min+self.dnu/2, self.nu_max, self.dnu)
        
        # units conversion
        self.dw = w0 * self.dnu / c
        self.w = w0 * ( 1 + self.nu/c ) # in AA
        self.fluxes *= ( 4 * np.pi * self.dnu / self.dw )
        self.images *= ( 4 * np.pi )
    
        return
        
        
    # This is to protect memory
    def load_cube(self, icam):
    
        # already loaded
        item = "cube%d"%icam
        if hasattr(self, item): return getattr(self, item)
        
        # out of range
        if icam>=self.ncam:
            print("Error: %d cameras only"%self.ncam)
            return
    
        # otherwise, read from file
        f = h5py.File(self.fname, 'r')
        cube = f["cubes"][icam] * ( 4 * np.pi * self.dnu / self.dw )
        setattr(self, "cube%d"%icam, cube)
        f.close()
        
        return cube
        
        
    # find pixel in an aperture
    def aperture(self, xc, yc, R):
    
        rc = np.sqrt( (self.x-xc)**2 + (self.y-yc)**2 )
        
        return np.where(rc<R)
        
        
    # extract spectrum from aperture
    def extract(self, icam, xc, yc, R):
    
        cube = self.load_cube(icam)
        i, j = self.aperture(xc, yc, R)
    
        return np.sum(cube[i,j,:], axis=0)


class Photon:

    def __init__(self, fname="photon", nfiles=1):
    
        N = 0
        for n in range(nfiles):
            f = h5py.File(fname+"_%d.h5"%n, 'r')
            N += f.attrs["n_escaped"]
            f.close()
        self.N = N # number of photon packets
        
        n_hat = np.zeros((N,3))
        p = np.zeros((N,3))
        nu = np.zeros(N)
        w = np.zeros(N)
        
        nL = 0
        for n in range(nfiles):
            f = h5py.File(fname+"_%d.h5"%n, 'r')
            nR = nL + f.attrs["n_escaped"]
            n_hat[nL:nR] = f["direction"][...]
            p[nL:nR] = f["position"][...] / kpc
            nu[nL:nR] = f["freq"][...]
            w[nL:nR] = f["weight"][...]
            f.close()
            nL = nR
            
        self.n_hat = n_hat
        self.p = p
        self.nu = nu
        self.w = w
    
        return
