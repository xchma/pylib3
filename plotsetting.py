from matplotlib import rc

rc('axes', linewidth=1, labelsize=18)
rc('xtick.major', size=6, width=1, pad=4)
rc('xtick.minor', size=3, width=1, pad=4)
rc('ytick.major', size=6, width=1, pad=4)
rc('ytick.minor', size=3, width=1, pad=4)
rc('legend', fontsize=18, frameon=False, labelspacing=0.3,
   handlelength=1, handletextpad=0.3, borderaxespad=0.3)
rc('xtick', direction='in', top=True, labelsize=18)
rc('ytick', direction='in', right=True, labelsize=18)
rc('font', size=18, family='DejaVu Sans', serif='Palatino')
rc('errorbar', capsize=3)
rc('text', usetex=True)
rc('lines', linewidth=2)
