import gizmo_lib.snapshot
import constant
import cosmology


# this returns a SnapGroup class
def loadsgrp(sdir, snum, cosmological=0):

    return gizmo_lib.snapshot.SnapGroup(sdir, snum, cosmological=cosmological)
    

# this returns a Snapshot class
def loadsnap(sdir, snum, cosmological=0):

    g = loadsgrp(sdir, snum, cosmological=cosmological)

    return g.loadsnap(snum)


# this returns an IC class
def loadic(fname):

    return gizmo_lib.snapshot.IC(fname)


def PhysicalConstant():

    return constant.PhysicalConstant()


def Cosmology(h=0.68, omega=0.31):

    return cosmology.Cosmology(h=h, omega=omega)


def Profile():

    return cosmology.Profile()
